
import random

# 1. 生成一个随机数
number = random.randint(1, 100)

# 2. 让用户输入一个数字
guess = int(input("请输入一个数字："))

# 3. 判断用户猜的数字是否正确
while int(guess) != number:
    # 如果猜的数字太小
    if int(guess) < number:
        print("你猜的数字太小了，再猜一次")

    # 如果猜的数字太大
    if int(guess) > number:
        print("你猜的数字太大了，再猜一次")

    # 继续让用户输入一个数字
    guess = int(input("请输入一个数字："))

# 如果猜对了，游戏结束
print("恭喜你，猜对了！")
