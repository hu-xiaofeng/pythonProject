from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class ApiPowerPage(WebKeys):
    def input_rolename(self, words):
        self.locator("xpath", "/html/body/section/div/div[1]/div/form/div[2]/div/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "/html/body/section/div/div[1]/div/form/div[4]/div/div/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "/html/body/section/div/div[1]/div/form/div[4]/div/div/button[2]").click()
        self.wait(1)

    def button_apipower(self):
        self.locator("xpath", "/html/body/section/div/div[2]/div/div[2]/div/div/div[2]/div[2]/table/tbody/tr[1]/td[10]/div/button[5]").click()
        self.wait(1)

    def input_apiname(self, words):
        self.locator("xpath", "/html/body/section/div/div[3]/div/div[2]/div/div[1]/div/input").send_keys(words)
        self.wait(1)

    def checkbox_apiname(self):
        self.locator("xpath", "/html/body/section/div/div[3]/div/div[2]/div/div[2]/div[11]/div[2]/div/div/label/span/span").click()
        self.wait(1)

    def button_comfirm(self):
        self.locator("xpath", "/html/body/section/div/div[3]/div/div[3]/span/button[2]").click()
        self.wait(1)

