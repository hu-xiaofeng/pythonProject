from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class ThemePage(WebKeys):

    # 新增主题
    def button_addtheme(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/button[1]").click()
        self.wait(1)

    def input_themecode(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[3]/div/div/div[2]/form/div[1]/div[1]/div/div/div[1]/div/input").send_keys(words)
        self.wait(1)

    def input_themename(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[3]/div/div/div[2]/form/div[1]/div[2]/div/div/div[1]/div/input").send_keys(words)
        self.wait(1)

    def input_themesort(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[3]/div/div/div[2]/form/div[2]/div[2]/div/div/div[1]/div/input").send_keys(words)
        self.wait(1)

    def input_themedescri(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[3]/div/div/div[2]/form/div[3]/div/div/div/textarea").send_keys(words)
        self.wait(1)

    def button_themecomfirm(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[3]/div/div/div[3]/span/button[2]").click()
        self.wait(1)

# 查询主题
    def input_searchname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/div/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[1]").click()

    def button_edit(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[2]/div/div/div[2]/div[2]/table/tbody/tr/td[7]/div/button[1]").click()
        self.wait(1)

    def input_editname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[3]/div/div/div[2]/form/div[1]/div[2]/div/div/div/div/input").clear()
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[3]/div/div/div[2]/form/div[1]/div[2]/div/div/div/div/input").send_keys(words)

    def button_removetheme(self):
        self.locator("xpath","//*[@id='app']/section/div/div[2]/div/div[2]/div/div/div[2]/div[2]/table/tbody/tr/td[7]/div/button[2]").click()
        self.wait(1)





