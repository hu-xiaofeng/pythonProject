from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class ZljkPage(WebKeys):
    def input_rulecode(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/div[2]/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[2]").click()
        self.wait(1)

    def button_fail(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[5]/button").click()
        self.wait(1)

    def icon_failclosed(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[5]/div/div/div[1]/button").click()
        self.wait(1)

    def icon_history(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/span/span/a/span").click()
        self.wait(1)

    def icon_startdate(self, words):
        self.locator("xpath", "/html/body/div[2]/div[1]/div/div/div/input[1]").send_keys(words)
        self.wait(1)

    def icon_enddate(self, words):
        self.locator("xpath", "/html/body/div[2]/div[1]/div/div/div/input[2]").send_keys(words)
        self.wait(1)

    def button_search(self):
        self.locator("xpath", "/html/body/div[2]/div[1]/button").click()
        self.wait(1)


    # 明细
    def button_view(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[21]/div/div/button").click()
        self.wait(1)

    def button_viewclosed(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[5]/div/div/div[1]/button").click()
        self.wait(1)

    # 分析
    def button_analysis(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[21]/div/button[1]").click()
        self.wait(1)

    def button_analysisclosed(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[5]/div[1]/div/div[1]/button").click()
        self.wait(1)

    # 跟踪
    def button_track(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[21]/div/button[2]").click()
        self.wait(1)

    # 通知方式
    def input_notice(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div[1]/div/div/form/div[1]/div/div/div[1]").click()
        self.up_keys()
        self.enter_keys()
        self.wait(1)

    def input_name(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div[1]/div/div/form/div[2]/div/div/div/input").send_keys(words)
        self.wait(1)
        self.down_keys()
        self.enter_keys()

    def input_textarea(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div[1]/div/div/form/div[3]/div/div[1]/textarea").send_keys(words)
        self.wait(1)

    def button_remarkcomfirm(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div[1]/div/div/div/button[1]").click()
        self.wait(1)

    def button_remarkback(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div[1]/div/div/div/button[2]").click()
        self.wait(1)


