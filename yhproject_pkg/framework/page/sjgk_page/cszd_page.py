# 从关键字文件导入基类
from yhproject_pkg.framework.key_word.keyword_web import WebKeys


from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class CszdPage(WebKeys):
    def input_search(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/input").send_keys(words)
        self.wait(1)

    def button_addcszd(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[1]/button").click()
        self.wait(1)

    def input_ysjbase(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/div/div[1]/div[2]/div[1]/input").send_keys(words)
        self.wait(1)

    def input_ysjtable(self, words):
        self.locator("xpath", "//*[@id='pane-direct']/div[1]/div/input").send_keys(words)
        self.wait(1)

    def button_search(self):
        self.locator("xpath", "//*[@id='pane-direct']/div[1]/button[1]").click()
        self.wait(1)

    def button_checktable(self):
        self.locator("xpath", "//*[@id='pane-direct']/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/div/label/span/span").click()
        self.wait(1)

    def button_comfirm(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[3]/div/button[2]").click()
        self.wait(1)

    def icon_closed(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[1]/button/i").click()
        self.wait(1)

# 编辑数据字段
    def local_paramsdict(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[3]/div[4]/div[2]/div/div[1]/div/div[1]").click()
        self.wait(3)

    def button_edit(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[3]/div[4]/div[2]/div/div[1]/div/div[2]/button[2]").click()
        self.wait(1)

    def button_remove(self):
        self.locator("xpath", "/html/body/div/section/div/div[1]/div[3]/div[4]/div[2]/div/div[1]/div/div[2]/button[1]").click()
        self.wait(1)

    def input_describe(self, word):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/form/div[3]/div[2]/div/input").send_keys(word)
        self.wait(1)

    def button_editsave(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[3]/div/button[2]").click()
        self.wait(1)




