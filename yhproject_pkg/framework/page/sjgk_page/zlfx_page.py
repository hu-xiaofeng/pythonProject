from yhproject_pkg.framework.key_word.keyword_web import WebKeys
import pyautogui


class ZlfxPage(WebKeys):
    def input_startdate(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/input[1]").send_keys(words)
        self.wait(1)

    def input_enddate(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/input[2]").send_keys(words)
        self.wait(1)

    def button_search(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/button[1]").click()
        self.wait(1)

    def date_query(self):
        pyautogui.click(156, 780)

    def button_rebackanalysis(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[3]").click()
        self.wait(1)

    def button_dataview(self):
        pyautogui.click(210, 1316)
        pyautogui.click(210, 1316)
        self.wait(2)

    def button_fieldview(self):
        pyautogui.click(430, 1316)
        pyautogui.click(430, 1316)
        self.wait(2)

    def button_tableview(self):
        pyautogui.click(640, 1316)
        pyautogui.click(640, 1316)
        self.wait(2)

    def button_dataclosed(self):
        pyautogui.click(2510, 911)
        self.wait(1)






