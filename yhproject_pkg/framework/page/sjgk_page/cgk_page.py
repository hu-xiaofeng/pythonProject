# 从关键字文件导入基类
from yhproject_pkg.framework.key_word.keyword_web import WebKeys


from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class CgkPage(WebKeys):
    def input_search(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath","//*[@id='app']/section/div/div[1]/div/div/button[1]").click()
        self.wait(1)

    def button_addcgk(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/button[1]").click()
        self.wait(1)

    def button_managecgk(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/button[2]").click()
        self.wait(1)

    def button_closemanage(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/button[3]").click()
        self.wait(1)

    def input_cgkchiname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/div/div[3]/table/tbody/tr/td[2]/div/div/input").send_keys(words)
        self.wait(1)

    def input_cgkenglishname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/div/div[3]/table/tbody/tr/td[3]/div/div/input").send_keys(words)
        self.wait(1)

    def button_cgksave(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[3]/div/button[2]").click()
        self.wait(1)

    def input_sort(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/div/div[3]/table/tbody/tr/td[9]/div/div/input").send_keys(words)
        self.wait(1)

    def button_edit(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr[1]/td[15]/div/div/button[1]/span/i").click()
        self.wait(1)

    def input_xlschinaname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[3]/table/tbody/tr[1]/td[3]/div/div/div/input").clear()
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[3]/table/tbody/tr[1]/td[3]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_xlsenglishname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[3]/table/tbody/tr[1]/td[4]/div/div/div/input").clear()
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[3]/table/tbody/tr[1]/td[4]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_xlssort(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[3]/table/tbody/tr[1]/td[10]/div/div/div/input").clear()
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[3]/table/tbody/tr[1]/td[10]/div/div/div/input").send_keys(words)
        self.wait(1)

    def button_comfirm(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr[1]/td[15]/div/div/button[1]").click()
        self.wait(1)

    def button_remove(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr[1]/td[15]/div/div/button[2]").click()
        self.wait(1)

