from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class BzfgPage(WebKeys):
    def icon_range(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/div/div/div/div/input").click()
        self.wait(1)

    def button_localsearch(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button").click()
        self.wait(2)

    def input_standardname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/button[1]").click()
        self.wait(3)

    def button_reset(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/button[2]").click()
        self.wait(1)

# 关联字段
    def icon_relatedfield(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[5]/div[2]/table/tbody/tr/td[9]/div/a").click()
        self.wait(1)

    def input_relatedfield(self, words):
        self.locator("xpath", "/html/body/div[3]/div/div/section/div/div[1]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "/html/body/div[3]/div/div/section/div/div[1]/button[1]").click()
        self.wait(1)

    def button_fieldreset(self):
        self.locator("xpath", "/html/body/div[3]/div/div/section/div/div[1]/button[2]").click()
        self.wait(1)

    def icon_fieldclosed(self):
        self.locator("xpath", "/html/body/div[3]/div/div/header/button/i").click()
        self.wait(1)

# 展开推荐关联
    def icon_recommendrelated(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[5]/div[2]/table/tbody/tr/td[11]/div/a").click()
        self.wait(1)

    def input_recommendsearch(self, words):
        self.locator("xpath", "/html/body/div[4]/div/div/section/div/div[1]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "/html/body/div[4]/div/div/section/div/div[1]/button[1]").click()
        self.wait(1)

    def icon_ownertable(self):
        self.locator("xpath", "/html/body/div[4]/div/div/section/div/div[2]/div[3]/table/tbody/tr/td[2]/div/a").click()
        self.wait(10)

    def icon_ownerfield(self):
        self.locator("xpath", "/html/body/div[4]/div/div/section/div/div[2]/div[3]/table/tbody/tr/td[3]/div/a").click()
        self.wait(3)

    def icon_recommendclosed(self):
        self.locator("xpath", "/html/body/div[4]/div/div/header/button/i").click()
        self.wait(1)

    def icon_ysjtabclosed(self):
        self.locator("xpath", "//*[@id='tab-yhdgp:controll:metaData:metaDataManage']/span").click()
        self.wait(1)

