# 数据权限管理
from yhproject_pkg.framework.key_word.keyword_web import WebKeys
import pyautogui


class SjqxMetaPage(WebKeys):
    def input_tables(self, words):
        self.locator("xpath", "//*[@id='pane-META_DATA']/div/div[1]/input").clear()
        self.locator("xpath", "//*[@id='pane-META_DATA']/div/div[1]/input").send_keys(words)
        self.wait(1)

    def local_database(self):
        self.locator("xpath", "//*[@id='pane-META_DATA']/div/div[2]/div[3]/div[2]/div[1]/div[1]/div/div/span").click()
        self.wait(1)

    def local_tables(self):
        self.locator("xpath", "//*[@id='pane-META_DATA']/div/div[2]/div[3]/div[2]/div[1]/div[2]/div/div[2]/div[3]/div[1]/div/div/span").click()
        self.wait(1)


class SjqxServicePage(WebKeys):
    def tab_datasever(self):
        self.locator("xpath", "//*[@id='tab-DATA_SERVICE']").click()
        self.wait(1)

    def input_tables(self, words):
        self.locator("xpath", "//*[@id='pane-DATA_SERVICE']/div/div[1]/input").clear()
        self.locator("xpath", "//*[@id='pane-DATA_SERVICE']/div/div[1]/input").send_keys(words)
        self.wait(1)

    def local_tables(self):
        self.locator("xpath", "//*[@id='pane-DATA_SERVICE']/div/div[2]/div[10]/div[2]/div[50]/div/div/div/span").click()
        self.wait(1)

    def tab_datarange(self):
        pyautogui.click(898, 408)
        self.wait(1)

    def input_fields(self, words):
        self.locator("xpath", "/html/body/div/section/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "/html/body/div[1]/section/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/button[1]").click()
        self.wait(1)

    def icon_role(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/div/div/input").click()
        self.wait(1)
        self.locator("xpath", "/html/body/div[2]/div[1]/div[1]/div[1]/ul/li[1]/label").click()
        self.wait(1)
        self.locator("xpath", "/html/body/div[1]/section/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/div/div/input").click()

    def button_edit(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/button").click()
        self.wait(1)

    def button_save(self):
        self.locator("xpath", "//*[@id='pane-row']/div[2]/div[1]/div[2]/button[2]").click()
        self.wait(1)

    def button_compare(self):
        self.locator("xpath", "//*[@id='pane-row']/div[4]/div[3]/table/tbody/tr/td[4]/div/div/div/input").click()
        self.up_keys()
        self.enter_keys()


class SjqxPage(WebKeys):
    def input_fields(self, words):
        self.locator("xpath", "//*[@id='pane-col']/div[1]/div[2]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='pane-col']/div[1]/div[2]/button[1]").click()

    def button_edit(self):
        self.locator("xpath", "//*[@id='pane-col']/div[2]/button").click()
        self.wait(1)

    def button_save(self):
        self.locator("xpath", "//*[@id='pane-col']/div[2]/div/button").click()
        self.wait(1)

    def button_sqlx(self):
        self.locator("xpath", "//*[@id='pane-col']/div[3]/div[3]/table/tbody/tr/td[6]/div/div/div/input").click()
        self.wait(1)
        self.up_keys()
        self.enter_keys()

    def checkbox_role(self):
        self.locator("xpath", "//*[@id='pane-col']/div[3]/div[3]/table/tbody/tr/td[7]/div/div/div[1]/input").click()
        self.wait(1)
        self.locator("xpath", "/html/body/div[3]/div[1]/div/div[1]/ul/li[1]/label/span/span").click()
        self.wait(1)







