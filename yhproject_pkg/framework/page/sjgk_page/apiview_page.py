# 接口审批
from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class ReviewPage(WebKeys):
    def input_apiname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/div[1]/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[2]").click()
        self.wait(1)

    def button_check(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[2]/div[3]/div[4]/div[2]/table/tbody/tr/td[9]/div/button[1]").click()
        self.wait(1)

    def button_view(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[2]/div[3]/div[4]/div[2]/table/tbody/tr/td[9]/div/button[2]").click()
        self.wait(1)

    def tab_checkd(self):
        self.locator("xpath", "//*[@id='tab-tabReviewed']").click()
        self.wait(1)

