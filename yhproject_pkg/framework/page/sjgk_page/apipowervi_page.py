from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class PowerViPage(WebKeys):
    def input_apiname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/div[1]/div/div/input").send_keys(words)
        self.wait(1)

    def input_username(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/div[3]/div/div/input").send_keys(words)
        self.wait(1)

    def button_search(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[2]").click()
        self.wait(1)

