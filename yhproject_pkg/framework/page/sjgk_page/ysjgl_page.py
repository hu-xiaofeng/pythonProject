# 从关键字文件导入基类
from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class YsjglPage(WebKeys):
    def icon_ysjcls(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div[1]/div/a").click()
        self.wait(1)
        self.locator("xpath", "/html/body/ul/li").click()
        self.wait(1)

    def button_ysjcls(self, word_keys, word_keys2):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/form/div[1]/div[2]/div[1]/input").send_keys(word_keys)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/form/div[2]/div[2]/div[1]/input").send_keys(word_keys2)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[3]/span/button[2]").click()
        self.wait(2)

# 选择元数据
    def input_searchcls(self, word_keys):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div[2]/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div[2]/input").send_keys(word_keys)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div[3]/div[13]/div[1]/div/span").click()
        self.wait(1)

    def local_datacls(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div[3]/div[13]/div[1]/div/span").click()
        self.wait(2)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div[3]/div[13]/div[1]/div/div/span").click()
        self.wait(1)

    def button_addruleset(self):
        self.locator("xpath", "/html/body/ul/li[2]").click()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[2]/div/div/div[1]/div/button/i").click()
        self.wait(1)

    def button_editdatacls(self):
        self.locator("xpath", "/html/body/ul/li[1]").click()
        self.wait(1)

    def button_removedatacls(self):
        self.locator("xpath", "/html/body/ul/li[3]").click()
        self.wait(1)

    def input_datacls(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/form/div[1]/div[2]/div/input").send_keys(words)
        self.wait(1)

    def button_dataclscomfirm(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[3]/div/div[3]/span/button[2]").click()
        self.wait(1)

    def input_ruleset(self, word_keys1, word_keys2):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[2]/div/div/div[3]/div/div/section/div/form/div[1]/div/div[1]/input").send_keys(word_keys1)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[2]/div/div/div[3]/div/div/section/div/form/div[2]/div/div/div/input").send_keys(word_keys2)
        self.wait(1)
        self.up_keys()
        self.wait(1)
        self.enter_keys()

    def button_alltable(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[2]/div/div/div[3]/div/div/section/div/form/div[3]/div[1]/div/div/label[1]/span").click()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[2]/div/div/div[3]/div/div/section/div/form/div[3]/div[2]/div/div[1]/p/label/span[1]/span").click()
        self.wait(1)

    def button_right(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[2]/div/div/div[3]/div/div/section/div/form/div[3]/div[2]/div/div[2]/button[2]").click()
        self.wait(1)

    def button_comfirm(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[2]/div/div/div[3]/div/div/section/div/div/button[2]").click()
        self.wait(1)

# 元数据查看
    def view_table(self, tablename, filedname):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[1]/div/div[1]/input").send_keys(tablename)
        self.wait(1)
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[1]/div/div[2]/input").send_keys(filedname)
        self.wait(1)
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[1]/button[1]").click()
        self.wait(1)

# 元数据分类编辑
    def edit_table(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[1]/td[7]/div/button[1]").click()
        self.wait(1)

    def input_tablechiname(self,tablename):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[1]/td[4]/div/div/input").send_keys(tablename)
        self.wait(1)

    def clear_tablechiname(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[1]/td[4]/div/div/input").clear()

    def button_tablecls(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[1]/td[6]/div/button").click()
        self.wait(1)

    def input_tableclsname(self, clsname):
        self.locator("xpath", "//*[@id='right']/div[2]/div/div/input").send_keys(clsname)
        self.wait(1)

    def check_tableclsname(self):
        self.locator("xpath", "//*[@id='right']/div[3]/div/div[1]/div[1]/label/span/span").click()
        self.wait(1)

    def button_tableclssave(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[11]/div/div[3]/span/button[2]").click()
        self.wait(1)

    def button_edsave(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[1]/td[7]/div/button[2]").click()
        self.wait(1)

# 元数据字段查看
    def view_field(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[1]/td[1]/div/div/i").click()
        self.wait(1)

# 元数据字段编辑
    def button_editfield(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[2]/td/div/div[5]/div[2]/table/tbody/tr/td[12]/div/button[2]").click()
        self.wait(1)

    def edit_fieldcls(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[2]/td/div/div[3]/table/tbody/tr/td[7]/div/button").click()
        self.wait(1)

    def input_datacode(self, datacode):
        xpath_datacode = "/html/body/div[1]/section/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div[11]/div/div[2]/div/div/div[2]/div[1]/div/input"
        self.locator("xpath", xpath_datacode).clear()
        self.locator("xpath", xpath_datacode).send_keys(datacode)
        self.locator("xpath","//*[@id='pane-tab1']/div/div[11]/div/div[2]/div/div/div[2]/div[1]/button[1]").click()
        self.wait(1)

    def reset_datacode(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[11]/div/div[2]/div/div/div[2]/div[1]/button[2]").click()
        self.wait(1)

    def checkbox_datacode(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[11]/div/div[2]/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr/td[6]/div/label").click()
        self.wait(1)

    def button_datacodesave(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[11]/div/div[3]/span/button[2]").click()
        self.wait(1)

    def button_fieldsave(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[2]/td/div/div[5]/div[2]/table/tbody/tr/td[12]/div/button[3]").click()

# 关联枚举值
    def button_editemun(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[2]/td/div/div[5]/div[2]/table/tbody/tr/td[12]/div/button[1]").click()
        self.wait(1)

    def input_dictcode(self, dictcode):
        self.locator("xpath", "/html/body/div[2]/div/div/section/div[1]/div/div/div[1]/div/div[2]/div/input").clear()
        self.locator("xpath", "/html/body/div[2]/div/div/section/div[1]/div/div/div[1]/div/div[2]/div/input").send_keys(dictcode)
        self.wait(1)
        self.locator("xpath", "/html/body/div[2]/div/div/section/div[1]/div/div/div[1]/div/div[2]/button[1]").click()
        self.wait(1)

    def checkbox_dictcode(self):
        self.locator("xpath", "/html/body/div[2]/div/div/section/div[1]/div/div/div[1]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td[1]/div/label/span[2]").click()
        self.wait(1)

    def button_dictcomfirm(self):
        self.locator("xpath", "/html/body/div[2]/div/div/section/div[2]/button[2]").click()
        self.wait(1)

# 滚动页面最右侧
    def scroll_right(self):
        js1 = 'document.getElementsByClassName("el-table__body-wrapper is-scrolling-left")[0].scrollLeft=1000000'
        self.driver.execute_script(js1)
        self.wait(3)

    def link_datadict(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[2]/td/div/div[3]/table/tbody/tr/td[11]/div").click()
        self.wait(1)

    def icon_closedictcode(self):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div[9]/div/div/header/button/i").click()
        self.wait(1)

# 字段注释
    def input_fieldremark(self, words):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[2]/td/div/div[3]/table/tbody/tr/td[2]/div/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[2]/td/div/div[3]/table/tbody/tr/td[2]/div/div/input").send_keys(words)
        self.wait(1)

    def button_fieldcomfirm(self):
        self.locator("xpath", "//*[@id='pane-tab1']/div/div[2]/div[3]/table/tbody/tr[2]/td/div/div[5]/div[2]/table/tbody/tr/td[12]/div/button[3]").click()
        self.wait(1)

#  历史操作记录
    def button_historyremark(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div[1]/button[2]").click()
        self.wait(1)

    def input_dataname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div[2]/form/div[3]/input").send_keys(words)
        self.wait(1)

    def input_tablename(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div[2]/form/div[4]/input").send_keys(words)
        self.wait(1)

    def input_fieldname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div[2]/form/div[5]/input").send_keys(words)
        self.wait(1)

    def button_historysearch(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div[2]/form/div[7]/div/button").click()
        self.wait(1)

    def button_historyback(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div[1]/button").click()

