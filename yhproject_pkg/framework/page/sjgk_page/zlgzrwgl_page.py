import string

from yhproject_pkg.framework.key_word.keyword_web import WebKeys
import pyautogui


class ZlgzrwPage(WebKeys):
    def button_addtask(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/button[1]").click()
        self.wait(1)

    def input_checkcode(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[2]/form/div[1]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_taskname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[2]/form/div[2]/div/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[2]/form/div[2]/div/div/input").send_keys(words)
        self.wait(1)

    def button_addcomfirm(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[3]/span/button[2]").click()
        self.wait(1)

    # 搜索
    def input_keycode(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div[2]/input").send_keys(words)
        self.wait(2)
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/div/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/div/button[2]").click()
        self.wait(2)

    # 执行sql
    def button_action(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[13]/div/button[1]").click()
        self.wait(1)

    # 启停
    def button_switch(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[13]/div/button[2]").click()
        self.wait(1)

    # 日志
    def button_log(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[13]/div/button[3]").click()
        self.wait(1)

    def icon_closelog(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[1]/button").click()
        self.wait(1)

    # 编辑
    def button_edit(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[13]/div/button[4]").click()
        self.wait(1)

    def input_taskeidt(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[2]/form/div[2]/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[3]/span/button[2]").click()
        self.wait(1)

    # 删除
    def button_remove(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[13]/div/button[5]").click()
        self.wait(1)




