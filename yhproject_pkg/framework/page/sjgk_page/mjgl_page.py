# 从关键字文件导入基类
from yhproject_pkg.framework.key_word.keyword_web import WebKeys


from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class MjglPage(WebKeys):
# 新增枚举值分类
    def button_addmjglcls(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[1]/div/div/div[1]/div/button").click()
        self.wait(1)

    def button_staticdict(self):
        self.locator("xpath", "/html/body/ul/li[1]").click()
        self.wait(1)

    def button_dynamicdict(self):
        self.locator("xpath", "/html/body/ul/li[2]").click()
        self.wait(1)

    def button_singledict(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/form/div[1]/div/div/div[1]").click()
        self.wait(1)

    def button_multidict(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/form/div[1]/div/div/div[2]").click()
        self.wait(1)

    def input_staticdictcode(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/form/div[2]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_staticdictname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/form/div[3]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_staticremark(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/form/div[4]/div/div/textarea").send_keys(words)
        self.wait(1)

    def button_addsave(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[3]/div/button[2]").click()
        self.wait(1)

    def input_dynamicdictcode(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/form/div[1]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_dynamicdictname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/form/div[2]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_dynamicdictremark(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[4]/div/div[2]/div/form/div[3]/div/div/textarea").send_keys(words)
        self.wait(1)

    def button_removecls(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[1]/div/div/div[3]/div[1]/div[3]/table/tbody/tr/td[5]/div/div/button[2]").click()
        self.wait(1)


# 新增枚举值

    def input_search(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[1]/div/div/div[2]/button[2]").click()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[1]/div/div/div[2]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[1]/div/div/div[2]/button[1]").click()
        self.wait(1)

    def local_dict(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[1]/div/div/div[3]/div[1]/div[3]/table/tbody/tr/td[2]/div").click()
        self.wait(1)

    def button_addstaticdict(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[1]/div[1]/div[2]/button[1]").click()
        self.wait(1)

    def input_sindictcode(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/form/div[1]/div/div[1]/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/form/div[1]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_sindictvalue(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/form/div[2]/div/div[1]/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/form/div[2]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_sindictremark(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/form/div[5]/div/div/textarea").send_keys(words)
        self.wait(1)

    def button_sindictsave(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[3]/div/button[2]").click()
        self.wait(1)

# 查询、编辑枚举值
    def input_searchsindict(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[1]/div/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[1]/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[1]/div/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[1]/div/button[2]").click()
        self.wait(1)

    def edit_sindict(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[2]/div[4]/div[2]/table/tbody/tr/td[6]/div/div/button[1]").click()
        self.wait(1)

    def input_editsindictcode(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/div/form/div[1]/div/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/div/form/div[1]/div/div/input").send_keys(words)
        self.wait(1)

    def input_eidtsindictvalue(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/div/form/div[2]/div/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/div/form/div[2]/div/div/input").send_keys(words)
        self.wait(1)

    def button_editsindictsave(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[3]/div/button[2]").click()
        self.wait(1)

    def remove_sindict(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[2]/div[4]/div[2]/table/tbody/tr/td[6]/div/div/button[2]").click()
        self.wait(1)

# 子层级
    def button_minidict(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[2]/div[4]/div[2]/table/tbody/tr/td[7]/div/div/button[1]").click()
        self.wait(1)

    def input_minicode(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/form/div[2]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_minieditcode(self,words):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/div/form/div[2]/div/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/div/form/div[2]/div/div/input").send_keys(words)
        self.wait(1)

    def input_minivalue(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/form/div[3]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_minieditvalue(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/div/form/div[3]/div/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[2]/div/form/div[3]/div/div/input").send_keys(words)
        self.wait(1)

    def input_miniremark(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/form/div[6]/div/div/textarea").send_keys(words)
        self.wait(1)

    def input_minisave(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[3]/div/button[2]").click()
        self.wait(1)

    def button_minieditsave(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[3]/div/button[2]").click()
        self.wait(1)

    def icon_expand(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[2]/div[3]/table/tbody/tr[1]/td[1]/div/div").click()
        self.wait(1)

    def button_miniedit(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[2]/div[4]/div[2]/table/tbody/tr[2]/td[7]/div/div/button[2]").click()
        self.wait(1)

    def button_miniremove(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[2]/div[4]/div[2]/table/tbody/tr[2]/td[7]/div/div/button[3]").click()
        self.wait(1)

    def button_minifaremove(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div[2]/div[4]/div[2]/table/tbody/tr/td[7]/div/div/button[3]").click()
        self.wait(1)

    # 动态字典
    def button_adddynamic(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[1]/div[1]/div[2]/button[2]").click()
        self.wait(3)

    def input_searchdynamic(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div/div/div[2]/div/input").send_keys(words)
        self.wait(2)

    def local_tablename(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div/div/div[3]/div[4]/div[2]/div/div[1]/div/div").click()
        self.wait(3)

    def input_keys(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div/div/input").click()
        self.down_keys()
        self.enter_keys()
        self.wait(1)

    def input_value(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/form/div[1]/div[3]/div/div/div/div/input").click()
        self.down_keys()
        self.enter_keys()
        self.wait(1)

    def input_conditionsql(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/form/div[2]/div/div/div/div/textarea").clear()
        self.wait(1)
        self.locator("xpath","//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/form/div[2]/div/div/div/div/textarea").send_keys(words)
        self.wait(1)

    def button_resetsql(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/form/div[3]/button[1]").click()
        self.wait(1)

    def button_viewdata(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/form/div[3]/button[2]").click()
        self.wait(1)

    def button_savedynamci(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/form/div[3]/button[3]").click()
        self.wait(3)

    def button_reback(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div/div/div[1]/div[1]/div[2]/button[3]").click()
        self.wait(1)






