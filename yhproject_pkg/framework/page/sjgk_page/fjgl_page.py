from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class FjglPage(WebKeys):

    # 新建树
    def button_treeadd(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div[1]/button").click()
        self.wait(2)

    def input_treerefer(self, word):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div/div/div[2]/form/div[1]/div/div/div/input").send_keys(word)
        self.wait(1)

    def input_treename(self, word):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div/div/div[2]/form/div[2]/div/div[1]/input").clear()
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div/div/div[2]/form/div[2]/div/div[1]/input").send_keys(word)
        self.wait(1)

    def input_treeremark(self, word):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div/div/div[2]/form/div[3]/div/div/textarea").send_keys(word)
        self.wait(1)

    def button_treesave(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div/div/div/div/div[1]/div/button[1]").click()
        self.wait(2)

    # 搜索树
    def input_treesearch(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[1]/div/div[2]/input").clear()
        self.locator("xpath", "/html/body/div/section/div/div[1]/div/div[2]/input").send_keys(words)
        self.wait(1)

    def local_tree(self):
        self.locator("xpath", "/html/body/div/section/div/div[1]/div/div[3]/div[3]/div[1]/div/span").click()
        self.wait(1)

    # 新建
    def button_addlevel(self):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div/div/div/div/div[3]/div[2]/button[3]").click()
        self.wait(1)

    def input_levelname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/div[1]/div[1]/div/div/div/input").clear()
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/div[1]/div[1]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_datafeature(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/div[1]/div[2]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_instance(self, words):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/div[1]/div[3]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_object(self, words):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/div[2]/div[1]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_scope(self, words):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/div[2]/div[2]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_extent(self, words):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/div[2]/div[3]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_import(self, words):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/div[3]/div[1]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_datalevel(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/div[3]/div[2]/div/div/div[1]/div/input").send_keys(words)
        self.wait(1)
        self.down_keys()
        self.enter_keys()

    def input_levelremark(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/div[3]/div[3]/div/div/div/input").send_keys(words)
        self.wait(1)

    def button_levelsave(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div/div/div[2]/div/div/div[3]/span/button[2]").click()
        self.wait(1)

# 分级查询和映射
    def input_searchlevel(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div/div/div/div/div[3]/div[2]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div/div/div/div/div[3]/div[2]/button[1]").click()
        self.wait(1)

    def button_updatelevel(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div/div/div/div/div[3]/div[3]/div[4]/div[2]/table/tbody/tr/td[10]/div/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div/div/div/div/div[3]/div[2]/button[2]").click()
        self.wait(1)

    def button_map(self):
        self.locator("xpath", "//*[@id='tab-tab2']").click()
        self.wait(1)

    def button_removelevel(self):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div/div/div/div/div[3]/div[3]/div[4]/div[2]/table/tbody/tr/td[10]/div/button[2]").click()
        self.wait(1)
        self.enter_keys()
        self.wait(1)

# 编辑分级树
    def local_oper(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/div[3]/div[3]/div[1]/div/div/span").click()
        self.wait(1)

    def button_edittree(self):
        self.locator("xpath", "/html/body/ul/li[1]").click()
        self.wait(1)

    def button_removetree(self):
        self.locator("xpath", "/html/body/ul/li[2]").click()
        self.wait(1)
