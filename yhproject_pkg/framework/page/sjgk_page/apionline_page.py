from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class OnlinePage(WebKeys):
    def input_apiname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/div[1]/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[1]").click()
        self.wait(1)

    def button_online(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div[2]/table/tbody/tr/td[9]/div/button[1]").click()
        self.wait(1)

    def button_view(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div[2]/table/tbody/tr/td[9]/div/button[2]").click()
        self.wait(1)

