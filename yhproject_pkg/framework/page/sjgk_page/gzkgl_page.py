import string

from yhproject_pkg.framework.key_word.keyword_web import WebKeys
import pyautogui


class GzkglPage(WebKeys):
    def button_highquery(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div/button[3]").click()
        self.wait(1)

    def box_query(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div[2]/div[1]/span[2]/div/label[3]/span[1]/span").click()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div[2]/div[2]/span[2]/div/label[1]/span[1]").click()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div[2]/div[3]/span[2]/div/label[2]/span[1]/span").click()
        self.wait(1)

    def input_checkcode(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div[1]/div[2]/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div[1]/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div[1]/button[2]").click()
        self.wait(1)

    # 停用
    def button_switch(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[18]/div/div/button").click()
        self.wait(1)

    # 编辑
    def button_eidt(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[18]/div/button[1]").click()
        self.wait(1)

    # 详情
    def button_view(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[5]/div[2]/table/tbody/tr/td[18]/div/button[2]").click()
        self.wait(1)

    # 删除
    def button_remove(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/button[1]").click()
        self.wait(1)

    # 检查规则名称
    def input_rulename(self, word):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[2]/div[1]/div[1]/form/div[2]/div/div/input").clear()
        self.wait(1)
        self.locator("xpath","//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[2]/div[1]/div[1]/form/div[2]/div/div/input").send_keys(word)
        self.wait(1)

    def button_editsave(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[2]/div[1]/div[2]/div[4]/button[2]").click()
        self.wait(1)

    def button_reback(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[2]/div/div[2]/div[4]/button").click()
        self.wait(1)

    def scroll_top(self):
        js = "var q=document.getElementsByClassName('sql')[0].scrollTop=10000"
        self.driver.execute_script(js)

    def box_check(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[4]/div[2]/table/tbody/tr/td[1]/div/label/span/span").click()
        self.wait(1)



