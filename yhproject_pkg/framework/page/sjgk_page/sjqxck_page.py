# 数据权限管理
from yhproject_pkg.framework.key_word.keyword_web import WebKeys
import pyautogui


class SjckPage(WebKeys):
    def input_tables(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[1]/div[1]/div[1]/input").send_keys(words)
        self.wait(1)

    def input_fields(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[1]/div[1]/div[2]/input").send_keys(words)
        self.wait(1)

    def button_search(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[1]/div[1]/button[1]").click()
        self.wait(1)

    def button_stop(self):
        self.locator("xpath", "/html/body/div/section/div/div/div/div/div/div[2]/div[5]/div[2]/table/tbody/tr/td[14]/div/div/button[1]").click()
        self.wait(1)

    def button_remove(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[2]/div[5]/div[2]/table/tbody/tr/td[14]/div/div/button[2]").click()
        self.wait(1)

    def tab_sjck(self):
        self.locator("xpath", "//*[@id='tab-yhdgp:controll:/dataSafemgr:perview']").click()
        self.wait(1)


class SjlsPage(WebKeys):
    def input_tables(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[1]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_fields(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[1]/div/div[2]/input").send_keys(words)
        self.wait(1)

    def button_search(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[1]/div/button[1]").click()
        self.wait(1)

    def tab_sjls(self):
        self.locator("xpath", "//*[@id='tab-yhdgp:controll:/dataSafemgr:perhistory']").click()
        self.wait(1)

