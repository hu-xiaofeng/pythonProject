# 从关键字文件导入基类
import string

from yhproject_pkg.framework.key_word.keyword_web import WebKeys
import pyautogui


class FlglPage(WebKeys):
    # 新增树
    def icon_opertree(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div[1]/div[2]/a/i").click()
        self.wait(1)

    def icon_addtree(self):
        self.locator("xpath", "/html/body/ul/li[1]").click()
        self.wait(3)

    def icon_removetree(self):
        self.locator("xpath", "/html/body/ul/li[3]").click()
        self.wait(2)

    def icon_addleaf(self):
        self.locator("xpath", "/html/body/ul/li[2]").click()
        self.wait(3)

    def input_clsname(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[2]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_clsremark(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[3]/div/div/textarea").send_keys(words)
        self.wait(1)

    def icon_addtitle(self):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[1]/div/span/button/span/i").click()
        self.wait(1)

    def input_title1(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div/div/div[2]/div/input").send_keys(words)
        self.wait(1)

    def input_titleremark1(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div/div/div[3]/div/input").send_keys(words)
        self.wait(1)

    def input_title2(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div[2]/div/div[2]/div/input").send_keys(words)
        self.wait(1)

    def input_titleremark2(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div[2]/div/div[3]/div/input").send_keys(words)
        self.wait(1)

    def input_title3(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div[3]/div/div[2]/div/input").send_keys(words)
        self.wait(1)

    def input_titleremark3(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div[3]/div/div[3]/div/input").send_keys(words)
        self.wait(1)

    def input_title4(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div[4]/div/div[2]/div/input").send_keys(words)
        self.wait(1)

    def input_titleremark4(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div[4]/div/div[3]/div/input").send_keys(words)
        self.wait(1)

    def input_title5(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div[5]/div/div[2]/div/input").send_keys(words)
        self.wait(1)

    def input_titleremark5(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div[5]/div/div[3]/div/input").send_keys(words)
        self.wait(1)

    def button_treesave(self):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[1]/div/button[2]").click()
        self.wait(3)

    def closed_tips(self):
        self.locator("xpath", "/html/body/div[2]/i[2]").click()
        self.wait(1)

    def input_leafname(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div/div[2]/div/div/div[2]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_leafremark(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div/div[2]/div/div/div[3]/div/div/textarea").send_keys(words)
        self.wait(1)

    def input_editleafremark(self, words):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div/div[2]/div/div[2]/div/div[2]/div/div/div/input").send_keys(words)
        self.wait(1)

    def button_leafsave(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[1]/div/button[2]").click()
        self.wait(2)

# 编辑树节点
    def button_edittree(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[1]/div/button[2]").click()
        self.wait(1)

# 编辑叶子
    def button_localleaf(self):
        self.locator("xpath", "/html/body/div/section/div/div[1]/div/div[2]/div[1]/div[1]/div/span").click()
        self.wait(1)

    def button_localleaf1(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/div[2]/div[1]/div[1]/div/span").click()
        self.wait(1)

    def button_editleaf(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[1]/div/button[2]").click()
        self.wait(1)

    # 新增树节点
    def icon_operleaf1(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/div[2]/div[1]/div[1]/div/div/span").click()
        self.wait(3)

    def button_addleaf1(self):
        self.locator("xpath", "/html/body/ul[2]/li[1]").click()
        self.wait(1)

    def button_adddata(self):
        self.locator("xpath", "/html/body/ul[2]/li[2]").click()
        self.wait(2)

    def button_chang(self):
        self.locator("xpath", "/html/body/ul[2]/li[3]").click()
        self.wait(1)

    def button_mount(self):
        self.locator("xpath", "/html/body/ul[2]/li[4]").click()
        self.wait(1)

    def button_remove(self):
        self.locator("xpath", "/html/body/ul[2]/li[5]").click()
        self.wait(1)

# 新增子分类
    def input_leafname1(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div/div[2]/div/div/div[2]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def button_leafsave1(self):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[1]/div/button[2]").click()
        self.wait(2)

# 新增数据项
    def input_dataitem1(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[2]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_dataremark1(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[3]/div/div/textarea").send_keys(words)
        self.wait(1)

# 主树数据项
    def input_datacode(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[1]/div[1]/div/div/div/input").clear()
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[1]/div[1]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_dataitem(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[1]/div[2]/div/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_dataremark(self, words):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[1]/div[3]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_level(self, words):
        el = "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[2]/div[1]/div/div/div/div/input"
        self.locator("xpath", el).click()
        self.locator("xpath", el).send_keys(words)
        self.down_keys()
        self.enter_keys()
        self.wait(1)

    def input_standard(self, words):
        el = "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[2]/div[2]/div/div/div/div/input"
        self.locator("xpath", el).click()
        self.locator("xpath", el).send_keys(words)
        self.down_keys()
        self.enter_keys()
        self.wait(1)

    def input_dataproduct(self, words):
        el = '/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[2]/div[3]/div/div/div/input'
        self.locator("xpath", el).send_keys(words)
        self.wait(1)

    def input_datacustomer(self, words):
        el = '/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[3]/div[1]/div/div/div/input'
        self.locator("xpath", el).send_keys(words)
        self.wait(1)

    def input_datatag(self, words):
        el = '/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[1]/div[2]/div/div/div[3]/div[2]/div/div/span/div/div[1]/input'
        self.locator("xpath", el).click()
        self.locator("xpath", el).send_keys(words)
        self.down_keys()
        self.enter_keys()
        self.wait(1)

    def input_datakey(self, words):
        el = "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div/div[1]/div/div[1]/div/div/div[1]/input"
        self.locator("xpath", el).send_keys(words)
        self.wait(1)

    def button_datasave(self):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div[1]/div/div[1]/div/button[2]").click()
        self.wait(1)

    def icon_adddatalist(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[1]/div/span/button/span/i").click()
        self.wait(1)

    def input_datakeys(self, words):
        el = "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div/div/div/div[1]/div/div/div[1]/input"
        self.locator("xpath", el).send_keys(words)
        self.wait(1)

    def input_datalocal(self, words):
        el = "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div/div/div/div[2]/div/div/div[1]/input"
        self.locator("xpath", el).send_keys(words)
        self.wait(1)

    def input_datauser(self, words):
        el = "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/form/div[2]/div[2]/div/div/div/div/div[3]/div/div/div[1]/input"
        self.locator("xpath", el).send_keys(words)
        self.wait(1)

    def icon_metadate(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[2]/div/div/div[1]/div[1]/div/span/button/span/i").click()
        self.wait(1)

    def input_metadata(self, words):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[3]/div/div/section/div[1]/div[1]/div[2]/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[3]/div/div/section/div[1]/div[1]/div[3]/div[3]/div[2]/div[1]/div[1]/div/span").click()
        self.wait(3)

    def input_tablename(self, words, words1):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[3]/div/div/section/div[1]/div[2]/div/div[2]/div[1]/div[1]/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[3]/div/div/section/div[1]/div[2]/div/div[2]/div[1]/div[2]/input").send_keys(words1)
        self.wait(1)
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[3]/div/div/section/div[1]/div[2]/div/div[2]/div[1]/button[1]").click()
        self.wait(10)

    def check_fields(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[3]/div/div/section/div[1]/div[2]/div/div[2]/div[2]/div[3]/table/tbody/tr/td[1]/div/div/i").click()
        self.wait(1)
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[3]/div/div/section/div[1]/div[2]/div/div[2]/div[2]/div[3]/table/tbody/tr[2]/td/div/div[4]/div[2]/table/tbody/tr[1]/td[1]/div/label/span/span").click()
        self.wait(1)

    def button_fieldsave(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[3]/div/div/section/div[3]/button[2]").click()
        self.wait(1)


# 调整层级
    def icon_firstitem(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[2]/div/div/section/div[1]/div/div/div[1]/div[1]/div/span").click()
        self.wait(1)

    def button_changsave(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/div/section/div[2]/button[2]").click()
        self.wait(1)

# 挂载
    def input_mountname(self, words):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[3]/div/div/section/div[1]/div/span/div/input").send_keys(words)
        self.wait(1)

    def button_firstmount(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[3]/div/div/section/div[1]/div/div/div[2]/div[1]/div/span").click()
        self.wait(1)
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[3]/div/div/section/div[2]/button[2]").click()
        self.wait(1)


# 切换主题
    def icon_theme(self):
        self.locator("xpath", "/html/body/div/section/div/div[1]/div/div[1]/div[1]/a/i").click()
        self.wait(1)


# 主树
    def input_datacode1(self, words):
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/span/div/input").clear()
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/span/div/input").send_keys(words)
        self.wait(1)

    def local_cls(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div[1]/div/span").click()
        self.wait(1)

    def icon_oprecls(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div[1]/div/div/span").click()
        self.wait(2)

    def button_adddata1(self):
        self.locator("xpath", "/html/body/ul[3]/li[2]").click()
        self.wait(5)

    def button_datasave1(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[2]/div/div[1]/div/div[1]/div/button[2]").click()
        self.wait(2)

# 删除数据项
    def local_datacode(self):
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[1]/div/span").click()
        self.wait(1)
        self.locator("xpath", "/html/body/div[1]/section/div/div[1]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[1]/div/div/span").click()
        self.wait(1)

    def remove_datacode(self):
        self.locator("xpath", "/html/body/ul[4]/li[5]").click()
        self.wait(1)


