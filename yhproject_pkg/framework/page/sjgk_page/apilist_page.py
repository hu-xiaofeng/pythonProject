from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class ApiListPage(WebKeys):
    def input_apiname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/div[1]/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[2]").click()
        self.wait(1)

    def button_viewsecret(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/button").click()
        self.wait(1)

    def button_secretclose(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div/div[1]/button").click()
        self.wait(1)

    def tab_apilist(self):
        self.locator("xpath", "//*[@id='tab-yhdgp:controll:dataServer:apiList']").click()
        self.wait(1)

    def tab_role(self):
        self.locator("xpath", "//*[@id='tab-ycmp:sys:basic:role']").click()
        self.wait(1)

    def button_view(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[4]/div[2]/table/tbody/tr/td[9]/div/button").click()
        self.wait(1)



