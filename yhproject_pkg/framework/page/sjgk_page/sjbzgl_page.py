# 从关键字文件导入基类
from pynput.keyboard import Controller
import pyautogui

from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class SjbzglPage(WebKeys):
    def button_addstanard(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[1]/button[1]").click()
        self.wait(1)

    def input_standardchinese(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/form/div[1]/div[1]/div/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_editchinese(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div[1]/form/div[1]/div[2]/div/div/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div[1]/form/div[1]/div[2]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_standardenglish(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/form/div[1]/div[2]/div/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_editenglish(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div[1]/form/div[2]/div[2]/div/div/div/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div[1]/form/div[2]/div[2]/div/div/div/input").send_keys(words)
        self.wait(1)

    def button_matchwords(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/form/div[1]/div[3]/button[1]").click()
        self.wait(1)

    def button_checkwords(self):
        self.locator("xpath", "/html/body/div[3]/div/div[2]/div[2]/label").click()
        self.wait(1)
        self.locator("xpath", "/html/body/div[3]/div/div[3]/span/button[2]").click()
        self.wait(1)

    def button_viewwords(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/form/div[1]/div[3]/button[2]").click()
        self.wait(1)

    def input_standardkeys(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/form/div[2]/div[1]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_dataclass(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/form/div[2]/div[2]/div/div/div/div/input").click()
        self.wait(1)

    def input_dataformat(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/form/div[3]/div[1]/div/div/div/div[1]/input").click()
        self.wait(1)

    def input_lengthconstrain(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/form/div[3]/div[2]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_accuracyconstrain(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/form/div[4]/div[1]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_relateddict(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/form/div[4]/div[2]/div/div/div/input").click()
        self.wait(2)

    def button_standardsave(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/div[1]/button[2]").click()
        self.wait(2)

# 查看词根
    def icon_closed(self):
        self.locator("xpath", "/html/body/div[2]/div/div/header/button/i").click()
        self.wait(1)

    def input_words(self, words):
        self.locator("xpath", "/html/body/div[2]/div/div/section/div/div[1]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "/html/body/div[2]/div/div/section/div/div[1]/button[1]").click()
        self.wait(1)

# 关联字典
    def input_dictcode(self, words):
        self.locator("xpath", "/html/body/div[6]/div/div/section/div[1]/div/div/div[1]/div/div[2]/div/input").send_keys(words)
        self.wait(1)

    def button_dictsearch(self):
        self.locator("xpath", "/html/body/div[6]/div/div/section/div[1]/div/div/div[1]/div/div[2]/button[1]").click()
        self.wait(1)

    def check_diccode(self):
        self.locator("xpath", "/html/body/div[6]/div/div/section/div[1]/div/div/div[1]/div/div[3]/div[1]/div[3]/table/tbody/tr/td[1]/div/label/span[1]/span").click()
        self.wait(1)

    def button_dictcomfirm(self):
        self.locator("xpath", "/html/body/div[6]/div/div/section/div[2]/button[2]").click()
        self.wait(1)

# 编辑
    def button_editstandard(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[2]/div/div[4]/div[2]/table/tbody/tr[1]/td[11]/div/button[1]").click()
        self.wait(1)

    def button_step(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div[1]/div[1]/button[2]").click()
        self.wait(1)

    def input_auditor(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div[2]/form/div[2]/div[1]/div/div/div/div/input").click()
        self.wait(1)

    def input_updatereason(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div[2]/form/div[3]/div/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_updateremark(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div[2]/form/div[4]/div/div/div/div/textarea").send_keys(words)
        self.wait(1)

    def button_updatecomfirm(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div[2]/div/button[2]").click()
        self.wait(1)

# 查询
    def input_searchstandard(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[1]/div[2]/input").send_keys(words)
        self.wait(1)

    def button_searchstandard(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[1]/button[4]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[1]/button[5]").click()
        self.wait(1)

# 配置
    def button_configure(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[2]/div/div[4]/div[2]/table/tbody/tr[1]/td[11]/div/button[2]").click()
        self.wait(2)

    def clear_configure(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/div[2]/div/div[2]/span[2]/span/i").click()
        self.wait(1)

    def input_clsdata(self, words):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/div[2]/div/div[1]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/div[2]/div/div[1]/button[1]").click()
        self.wait(2)

    def icon_checkdata(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div/div[2]/div/div[3]/div[4]/div[2]/table/tbody/tr/td[1]/div/label/span/span").click()
        self.wait(1)


    def button_clssave(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[3]/span/button[2]/span").click()
        self.wait(2)

# 查看
    def button_viewdata(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[2]/div/div[4]/div[2]/table/tbody/tr/td[11]/div/button[3]").click()
        self.wait(1)

    def down_mouse0(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[2]/div[1]/div[1]/div[1]/span").click()
        self.wait(1)
        self.down_mouse()
        self.wait(1)

    def button_closeddata(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[3]/span/button").click()
        self.wait(1)

# 启停
    def button_switch(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[2]/div/div[4]/div[2]/table/tbody/tr[1]/td[11]/div/button[4]/span/span/span").click()
        self.wait(1)

    def button_switch1(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[2]/div/div[4]/div[2]/table/tbody/tr[1]/td[11]/div/button[2]/span/span/span").click()
        self.wait(1)

# 变更记录管理
    def tab_updatarecord(self):
        self.locator("id", "tab-second").click()
        self.wait(1)

    def input_searchstandrad(self, words):
        self.locator("xpath", "//*[@id='pane-second']/div/div[1]/form/div[1]/div/div/input").send_keys(words)
        self.wait(1)

    def button_search(self):
        self.locator("xpath", "//*[@id='pane-second']/div/div[1]/form/div[3]/div/button").click()
        self.wait(1)

    def button_updatelist(self):
        self.locator("xpath", "//*[@id='pane-second']/div/div[2]/div/div[4]/div[2]/table/tbody/tr[1]/td[11]/div/button").click()
        self.wait(1)

    def icon_updateclosed(self):
        self.locator("xpath", "//*[@id='pane-second']/div/div[4]/div/div[1]/button").click()
        self.wait(1)
