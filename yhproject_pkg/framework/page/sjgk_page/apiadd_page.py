# 接口创建
from yhproject_pkg.framework.key_word.keyword_web import WebKeys
import pyautogui as ui
ui.PAUSE = 1


class ApiPage(WebKeys):
    def button_addapi(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/button").click()
        self.wait(1)

    def button_localapi(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/span[1]").click()
        self.wait(1)

    def button_outerapi(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/span[2]").click()
        self.wait(1)

    def button_miniapi(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/span[3]").click()
        self.wait(2)

    def input_searchapi(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/div[1]/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[1]").click()
        self.wait(2)

    def button_resetapi(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[2]").click()
        self.wait(1)

    def icon_todo(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[4]/div[2]/table/tbody/tr/td[9]/div/div/span").click()
        self.wait(1)

    def button_update(self):
        self.locator("xpath", "/html/body/ul/li[1]/button").click()
        self.wait(1)

    def button_test(self):
        self.locator("xpath", "/html/body/ul/li[2]/button").click()
        self.wait(1)

    def button_remove(self):
        self.locator("xpath", "/html/body/ul/li[3]/button").click()
        self.wait(1)

    def input_editapiname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/form/div[3]/div[1]/div/div/div/div/input").send_keys(words)
        self.wait(1)

    def button_commit(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div[4]/div[2]/table/tbody/tr/td[9]/div/button").click()
        self.wait(1)

    def button_reback(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div[2]/div[1]/div/div/form/div/div[2]/div/div[10]/button").click()
        self.wait(1)


class LocalApiPage(WebKeys):
    def input_datasource(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/form/div[1]/div[2]/div/div[1]/div/div/div/input").send_keys(words)
        self.wait(1)
        self.down_keys()
        self.enter_keys()

    def tab_sqlys(self):
        self.locator("xpath", "//*[@id='tab-sqlYS']").click()
        self.wait(1)

    def sql_sqledit(self):
        ui.click(565, 582)
        self.wait(1)

    def input_sql(self):
        ui.typewrite('SELECT\n\t T1."RULE_CODE", T1."IS_VALID"\n\t FROM\n\t "HUXIAOFENG"."thirdcycle_oracle_111" T1', interval=0.2)

    def button_apitest(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/form/div[2]/div/div[2]/div[1]/div/button[2]").click()
        self.wait(3)

    def icon_close(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[5]/div/div/div[1]/button").click()
        self.wait(1)

    def input_apiname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/form/div[3]/div[1]/div/div/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_themename(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/form/div[3]/div[3]/div/div/div/div/div[1]/div[1]/div[2]/input").send_keys(words)
        self.wait(1)
        self.enter_keys()
        self.wait(1)

    def input_uri(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/form/div[3]/div[5]/div/div/div/div[1]/input").send_keys(words)
        self.wait(1)

    def button_save(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[3]/div/button[2]").click()
        self.wait(1)


class MicroApiPage(WebKeys):
    def input_apiname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/div/form/div/div[2]/div[2]/div[1]/div/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_uri(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/div/form/div/div[2]/div[3]/div[2]/div/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_theme(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/div/form/div/div[2]/div[4]/div[1]/div/div/div/div[1]/div[1]/div[2]/input").send_keys(words)
        self.wait(1)
        self.down_keys()
        self.enter_keys()

    def input_url(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/div/form/div/div[2]/div[7]/div/div/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_request(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/div/form/div/div[2]/div[8]/div/div/div/div/div/input").click()
        self.wait(1)

    def button_apitest(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[3]/div/button[2]").click()
        self.wait(1)

    def button_testapitest(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div[2]/div[1]/div/div/form/div/div[2]/div/div[2]/div/button[2]").click()
        self.wait(1)

    def button_testapitest1(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/form/div/div[2]/div/div[2]/div/button[2]/span").click()
        self.wait(1)

    def button_testapitest2(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[3]/div/div[2]/div[1]/div/div/form/div/div[2]/div/div[2]/div/button[2]").click()
        self.wait(1)

    def button_testapitest3(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div[2]/div[1]/div/div/form/div/div[2]/div/div[2]/div/button[2]").click()
        self.wait(1)

    def button_reback(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div[2]/div[1]/div/div/form/div/div[2]/div/div[10]/button").click()
        self.wait(1)

    def button_reback1(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/form/div/div[2]/div/div[10]/button").click()
        self.wait(1)

    def button_reback2(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[3]/div/div[2]/div[1]/div/div/form/div/div[2]/div/div[10]/button").click()
        self.wait(1)

    def button_reback3(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div/div[2]/div[1]/div/div/form/div/div[2]/div/div[10]/button").click()
        self.wait(1)

    def tab_params(self):
        self.locator("xpath", "//*[@id='tab-Params']").click()
        self.wait(1)

    def tab_body(self):
        self.locator("xpath", "//*[@id='tab-Body']").click()
        self.wait(1)

    def input_body(self, words):
        self.locator("xpath", "//*[@id='pane-Body']/div[2]/div/div/div[6]/div[1]").clear()
        self.wait(1)

    def input_key(self, words):
        self.locator("xpath", "//*[@id='pane-Params']/div/div[3]/table/tbody/tr/td[1]/div/div/input").send_keys(words)
        self.wait(1)

    def input_values(self, words):
        self.locator("xpath", "//*[@id='pane-Params']/div/div[3]/table/tbody/tr/td[2]/div/div/input").send_keys(words)
        self.wait(1)

    def button_run(self):
        self.locator("xpath", "/html/body/div[3]/div/div[2]/div/div[1]/button").click()
        self.wait(1)

    def button_close(self):
        self.locator("xpath", "/html/body/div[3]/div/div[3]/span/button").click()
        self.wait(1)

    def button_save(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[3]/div/button[3]").click()
        self.wait(1)


class OuterApiPage(WebKeys):
    def input_apiname(self, words):
        self.locator("xpath",
                     "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/div/form/div/div[2]/div[2]/div[1]/div/div/div[1]/input").send_keys(
            words)
        self.wait(1)

    def input_uri(self, words):
        self.locator("xpath",
                     "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/div/form/div/div[2]/div[3]/div[2]/div/div/div[1]/input").send_keys(
            words)
        self.wait(1)

    def input_theme(self, words):
        self.locator("xpath",
                     "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/div/form/div/div[2]/div[4]/div[1]/div/div/div/div[1]/div[1]/div[2]/input").send_keys(
            words)
        self.wait(1)
        self.enter_keys()

    def input_url(self, words):
        self.locator("xpath",
                     "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/div/form/div/div[2]/div[7]/div/div/div/div[1]/input").send_keys(
            words)
        self.wait(1)

    def input_request(self):
        self.locator("xpath",
                     "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[2]/div/div/form/div/div[2]/div[8]/div/div/div/div/div/input").click()
        self.wait(1)

    def button_apitest(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[3]/div/button[2]").click()
        self.wait(1)

    def tab_params(self):
        self.locator("xpath", "//*[@id='tab-Params']").click()
        self.wait(1)

    def tab_headers(self):
        self.locator("xpath", "//*[@id='tab-Header']").click()
        self.wait(1)

    def input_hkey(self, words):
        self.locator("xpath", "//*[@id='pane-Header']/div/div[3]/table/tbody/tr/td[1]/div/div/input").send_keys(words)
        self.wait(1)

    def input_hvalues(self, words):
        self.locator("xpath", "//*[@id='pane-Header']/div/div[3]/table/tbody/tr/td[2]/div/div/input").send_keys(words)
        self.wait(1)

    def input_key(self, words):
        self.locator("xpath", "//*[@id='pane-Params']/div/div[3]/table/tbody/tr/td[1]/div/div/input").send_keys(words)
        self.wait(1)

    def input_values(self, words):
        self.locator("xpath", "//*[@id='pane-Params']/div/div[3]/table/tbody/tr/td[2]/div/div/input").send_keys(words)
        self.wait(1)

    def button_run(self):
        self.locator("xpath", "/html/body/div[3]/div/div[2]/div/div[1]/button").click()
        self.wait(5)

    def button_close(self):
        self.locator("xpath", "/html/body/div[3]/div/div[3]/span/button").click()
        self.wait(1)

    def button_save(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[4]/div[1]/div/div[3]/div/button[3]").click()
        self.wait(1)