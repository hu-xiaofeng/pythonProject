from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class SjtmMetaPage(WebKeys):
    def input_tables(self, words):
        self.locator("xpath", "//*[@id='pane-META_DATA']/div/div[1]/input").clear()
        self.locator("xpath", "//*[@id='pane-META_DATA']/div/div[1]/input").send_keys(words)
        self.wait(1)

    def local_database(self):
        self.locator("xpath", "//*[@id='pane-META_DATA']/div/div[2]/div[3]/div[2]/div[1]/div[1]/div/div/span").click()
        self.wait(1)

    def local_tables(self):
        self.locator("xpath", "//*[@id='pane-META_DATA']/div/div[2]/div[3]/div[2]/div[1]/div[2]/div/div[2]/div[3]/div[1]/div/div/span").click()
        self.wait(1)


class SjtmServicePage(WebKeys):
    def tab_datasever(self):
        self.locator("xpath", "//*[@id='tab-DATA_SERVICE']").click()
        self.wait(1)

    def input_tables(self, words):
        self.locator("xpath", "//*[@id='pane-DATA_SERVICE']/div/div[1]/input").clear()
        self.locator("xpath", "//*[@id='pane-DATA_SERVICE']/div/div[1]/input").send_keys(words)
        self.wait(2)

    def local_tables(self):
        self.locator("xpath", "//*[@id='pane-DATA_SERVICE']/div/div[2]/div[10]/div[2]/div[50]/div/div/div/span").click()
        self.wait(1)


class SjtmPage(WebKeys):
    def input_fields(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div/div[2]/div/div/div[1]/div[2]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div/div[2]/div/div/div[1]/div[2]/button[1]").click()
        self.wait(1)

    def button_editrule(self):
        self.locator("xpath","//*[@id='app']/section/div/div/div[2]/div/div/div[2]/div[3]/table/tbody/tr/td[7]/div/div/button").click()
        self.wait(1)

    def button_editserverule(self):
        self.locator("xpath","//*[@id='app']/section/div/div/div[2]/div/div/div[2]/div[3]/table/tbody/tr/td[5]/div/div/button").click()
        self.wait(1)

    def icon_addrule(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/div[2]/div[2]/div").click()
        self.wait(1)

    def input_role(self, words):
        self.locator("xpath",
                     "/html/body/div[1]/section/div/div[2]/div/div[2]/div/div[2]/div[2]/div[1]/div[1]/input").send_keys(
            words)
        self.wait(2)
        self.locator("xpath",
                     "//*[@id='app']/section/div/div[2]/div/div[2]/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/label/span/span").click()
        self.wait(1)

    def input_rule(self, words):
        self.locator("xpath",
                     "//*[@id='app']/section/div/div[2]/div/div[2]/div/div[2]/div[2]/div[2]/div[1]/input").send_keys(
            words)
        self.wait(1)

    def button_addrule(self):
        self.locator("xpath",
                     "//*[@id='app']/section/div/div[2]/div/div[2]/div/div[2]/div[2]/div[2]/div[2]/div/div[4]/div[2]/table/tbody/tr/td[5]/div/button").click()
        self.wait(1)

    def button_save(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/div[2]/div[3]/button[2]").click()
        self.wait(1)

    def icon_closed(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/button").click()
        self.wait(1)

    def icon_removerule(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[2]/div/div[2]/div[2]/div[1]/div[1]/div[2]").click()
        self.wait(1)
        self.enter_keys()
        self.wait(1)








