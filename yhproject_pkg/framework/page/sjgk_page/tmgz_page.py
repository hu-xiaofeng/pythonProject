from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class TmgzPage(WebKeys):
    def button_addrule(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[1]/div[2]/button[1]").click()
        self.wait(1)

    def input_rulename(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[4]/div/div[2]/div/form/div[1]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def input_expression(self, words1, words2):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[4]/div/div[2]/div/form/div[4]/div/div[1]/div/input").send_keys(words1)
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[4]/div/div[2]/div/form/div[4]/div/div[2]/div/input").send_keys(words2)
        self.wait(1)

    def input_replace(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[4]/div/div[2]/div/form/div[5]/div/div[1]/input").send_keys(words)
        self.wait(1)

    def button_save(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[4]/div/div[3]/div/button[2]").click()
        self.wait(1)

    def input_searchrule(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[1]/div[1]/div[1]/input").send_keys(words)
        self.wait(2)
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[1]/div[1]/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[1]/div[1]/button[2]").click()
        self.wait(1)

    def button_edit(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[2]/div[5]/div[2]/table/tbody/tr/td[14]/div/div/button[3]").click()
        self.wait(1)

    def input_editrule(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[5]/div/div[2]/div/form/div[1]/div/div/input").send_keys(words)
        self.wait(1)

    def button_editsave(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[5]/div/div[3]/div/button[2]").click()
        self.wait(3)

    def button_stop(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[2]/div[5]/div[2]/table/tbody/tr/td[14]/div/div/button[1]").click()
        self.wait(1)

    def button_recommand(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[2]/div[5]/div[2]/table/tbody/tr/td[14]/div/div/button[2]").click()
        self.wait(1)

    def button_remove(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div[2]/div[5]/div[2]/table/tbody/tr/td[14]/div/div/button[4]").click()
        self.wait(1)






