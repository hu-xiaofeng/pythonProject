from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class ApiLogPage(WebKeys):
    def input_username(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/div[2]/div/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/form/button[2]").click()
        self.wait(1)

    def button_review(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div[2]/table/tbody/tr[1]/td[10]/div/button").click()
        self.wait(1)

    def button_close(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[3]/div/div[3]/span/button").click()
        self.wait(1)


