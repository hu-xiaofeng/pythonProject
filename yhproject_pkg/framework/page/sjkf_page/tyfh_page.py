# 从关键字文件导入基类
from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class DataCheck(WebKeys):
    def button_search(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[1]/form/div[3]/div/button").click()
        self.wait(1)

    def button_view(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[2]/div/div[5]/div[2]/table/tbody/tr/td[19]/div/button[1]").click()
        self.wait(1)

    def button_cloesed(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[4]/div/div[1]/button").click()
        self.wait(1)

    def button_changer(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[2]/div/div[5]/div[2]/table/tbody/tr/td[19]/div/button[2]").click()
        self.wait(1)

    def button_pass(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[2]/div/div[5]/div[2]/table/tbody/tr/td[19]/div/button[3]").click()
        self.wait(1)

    def button_false(self):
        self.locator("xpath", "//*[@id='pane-first']/div/div[2]/div/div[5]/div[2]/table/tbody/tr/td[19]/div/button[4]").click()
        self.wait(1)


    def tab_tyfh(self):
        self.locator("xpath", "//*[@id='tab-yhdgp:dev:exploration:explorationexamine']").click()
        self.wait(1)

    def tab_history(self):
        self.locator("xpath", "//*[@id='tab-second']").click()
        self.wait(1)

    def button_hisview(self):
        self.locator("xpath", "//*[@id='pane-second']/div/div[2]/div/div[5]/div[2]/table/tbody/tr[1]/td[22]/div/button").click()
        self.wait(1)

    def icon_closed(self):
        self.locator("xpath", "//*[@id='pane-second']/div/div[4]/div/div[1]/button").click()
        self.wait(1)





