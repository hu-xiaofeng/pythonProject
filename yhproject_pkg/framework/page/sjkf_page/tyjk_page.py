# 从关键字文件导入基类
from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class DataMonitor(WebKeys):
    def input_datalist(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div[2]/input").send_keys(words)
        self.wait(1)

    def local_data(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div[3]/div[3]/div[2]/div[1]/div[1]/div/div").click()
        self.wait(1)

    def input_tablename(self, words):
        self.locator("xpath", "//*[@id='pane-tabs1']/div/div[1]/div[1]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='pane-tabs1']/div/div[1]/button[1]").click()

    def button_online(self):
        self.locator("xpath", "//*[@id='pane-tabs1']/div/div[1]/button[3]").click()
        self.wait(1)

    def button_monitorset(self):
        self.locator("xpath", "//*[@id='pane-tabs1']/div/div[1]/button[4]").click()
        self.wait(1)

    def icon_expand(self):
        self.locator("xpath", "//*[@id='pane-tabs1']/div/div[2]/div[3]/table/tbody/tr/td[1]/div/div/i").click()
        self.wait(1)

    def button_dataedit(self):
        self.locator("xpath", "//*[@id='pane-tabs1']/div/div[2]/div[3]/table/tbody/tr[1]/td[7]/div/button/span").click()
        self.wait(1)

    def tab_version(self):
        self.locator("xpath", "//*[@id='tab-tabs2']").click()
        self.wait(1)

    def button_versionview(self):
        self.locator("xpath", "//*[@id='pane-tabs2']/div/div[1]/div[3]/table/tbody/tr[1]/td[6]/div/button[2]").click()
        self.wait(1)

    def icon_checkitems(self):
        self.locator("xpath", "//*[@id='pane-tabs2']/div/div[3]/div/div[2]/div/div[1]/div/div/div/div/span/span/i").click()
        self.wait(1)
        self.down_keys()
        self.enter_keys()

    def input_tables(self, words):
        self.locator("xpath", "//*[@id='pane-tabs2']/div/div[3]/div/div[2]/div/div[1]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='pane-tabs2']/div/div[3]/div/div[2]/div/div[1]/button[1]").click()
        self.wait(1)

    def icon_expand1(self):
        self.locator("xpath", "//*[@id='pane-tabs2']/div/div[3]/div/div[2]/div/div[2]/div[3]/table/tbody/tr[1]/td[1]/div/div/i").click()
        self.wait(1)

    def icon_closed(self):
        self.locator("xpath", "//*[@id='pane-tabs2']/div/div[3]/div/div[1]/button").click()
        self.wait(1)

    def tab_monitor(self):
        self.locator("xpath", "//*[@id='tab-tabs1']").click()
        self.wait(1)

    def tab_tasklist(self):
        self.locator("xpath", "//*[@id='tab-tabs3']").click()
        self.wait(1)

    def button_edit(self):
        self.locator("xpath", "//*[@id='pane-tabs3']/div/div[1]/div[4]/div[2]/table/tbody/tr[1]/td[8]/div/button").click()
        self.wait(1)

    def button_addtask(self):
        self.locator("xpath", "//*[@id='pane-tabs3']/div/div[3]/div/div[3]/span/span/button[2]").click()
        self.wait(1)

    def icon_closed1(self):
        self.locator("xpath", "//*[@id='pane-tabs3']/div/div[3]/div/div[1]/button/i").click()
        self.wait(1)


