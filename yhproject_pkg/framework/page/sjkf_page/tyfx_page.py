# 从关键字文件导入基类
from yhproject_pkg.framework.key_word.keyword_web import WebKeys
from selenium.webdriver.common.action_chains import ActionChains
import pyautogui as ui

ui.PAUSE = 1


class DataList(WebKeys):
    def input_datasource(self, words):
        self.locator("xpath", "//*[@id='pane-tree1']/div[1]/input").clear()
        self.locator("xpath", "//*[@id='pane-tree1']/div[1]/input").send_keys(words)
        self.wait(1)

    def local_datasource(self):
        self.locator("xpath", "//*[@id='pane-tree1']/div[2]/div[2]/div[2]/div[2]/div[1]/div/div").click()
        self.wait(1)

    def tab_tree1(self):
        self.locator("xpath", "//*[@id='tab-tree1']").click()
        self.wait(1)

    def tab_monitor(self):
        self.locator("xpath", "//*[@id='tab-yhdgp:dev:exploration:explorationmonitor']").click()
        self.wait(1)

    def tab_check(self):
        self.locator("xpath", "//*[@id='tab-yhdgp:dev:exploration:explorationexamine']").click()
        self.wait(1)

    def tab_analysis(self):
        self.locator("xpath", "//*[@id='tab-yhdgp:controll:exploration:dataexploration']").click()
        self.wait(1)


class DataView(WebKeys):
    def input_tablename(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[2]/div[1]/div[1]/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[2]/div[2]/button[1]").click()
        self.wait(1)

    def button_reset(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[2]/div[2]/button[2]").click()
        self.wait(1)

    def checkbox_datalist(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[3]/div/div[4]/div[1]/table/thead/tr/th[1]/div/label/span/span").click()
        self.wait(1)

    def button_addtask(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[2]/button[1]").click()
        self.wait(1)

    def button_editdata(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[2]/button[3]").click()
        self.wait(1)

    def input_themename(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[5]/div/div[2]/div/form/div[1]/div/div/div/input").send_keys(words)
        self.wait(1)
        self.enter_keys()

    def button_taskcomfirm(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[5]/div/div[3]/span/span/button[2]").click()
        self.wait(1)

    def button_viewtable(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div/div[3]/div/div[5]/div[2]/table/tbody/tr/td[12]/div/button[1]").click()
        self.wait(1)

    def button_closed(self):
        self.locator("xpath", "//*[@id='el-drawer__title']/button").click()
        self.wait(1)

    def scroll_left(self):
        ui.click(2110, 1240)
        ui.dragTo(2510, 1240, 1)


class DataEdit(WebKeys):
    def button_tableedit(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[3]/div/div[5]/div[2]/table/tbody/tr/td[13]/div/button[1]").click()
        self.wait(1)

    def button_fieldedit(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[3]/div/div[5]/div[2]/table/tbody/tr/td[13]/div/button[2]").click()
        self.wait(1)

    def button_dataview(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[3]/div/div[5]/div[2]/table/tbody/tr/td[13]/div/button[3]").click()
        self.wait(1)

    def button_commit(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[3]/div/div[5]/div[2]/table/tbody/tr/td[13]/div/button[4]").click()
        self.wait(1)

    def button_export(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[3]/div/div[5]/div[2]/table/tbody/tr/td[13]/div/button[5]").click()
        self.wait(1)

    def button_uncommit(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[3]/div/div[5]/div[2]/table/tbody/tr/td[13]/div/button[6]").click()
        self.wait(1)

    def input_tablechinese(self, words):
        self.locator("xpath",
                     "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[2]/div/form/div[1]/div[2]/div/div/div/input").clear()
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[2]/div/form/div[1]/div[2]/div/div/div/input").send_keys(words)
        self.wait(1)

    def input_tableods(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[2]/div/form/div[1]/div[3]/div/div/div/input").send_keys(words)
        self.wait(1)

    def button_comfirm(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[3]/span/span/button[1]").click()
        self.wait(1)

    def button_addfield(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[2]/div[1]/button").click()
        self.wait(1)

    def input_field(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]/div/div/input").send_keys(words)
        self.wait(1)

    def input_fieldname(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div/div/input").send_keys(words)
        self.wait(1)

    def input_fieldtype(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[4]/div/div/div/input").send_keys(words)
        self.wait(1)
        self.down_keys()
        self.enter_keys()
        self.wait(1)

    def input_length(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[5]/div/div/input").send_keys(words)
        self.wait(1)

    def button_fieldcomfirm(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[3]/span/span/button[1]").click()
        self.wait(1)

    def button_eidtswitchtype(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[2]/div[2]/div/div[2]/table/thead/tr/th[4]/div/div/span").click()
        self.wait(1)
        self.locator("xpath", "/html/body/ul/li").click()
        self.wait(1)

    def button_odsclose(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[4]/div/div[1]/button").click()
        self.wait(1)

    def input_classfy(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[3]/div/div[3]/table/tbody/tr/td[5]/div/div/input").send_keys(words)
        self.wait(1)

    def input_classview(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[3]/div/div[3]/table/tbody/tr/td[6]/div/div/input").send_keys(words)
        self.wait(1)

    def box_check(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[3]/div/div[4]/div[2]/table/tbody/tr/td[1]/div/label/span/span").click()
        self.wait(1)

    def button_savedraft(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[1]/button[2]").click()
        self.wait(1)

    def button_reback(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[2]/div/div[1]/div[1]/button[1]").click()
        self.wait(1)


