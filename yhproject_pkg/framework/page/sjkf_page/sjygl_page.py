import pyautogui

from yhproject_pkg.framework.key_word.keyword_web import WebKeys
from selenium.webdriver.common.keys import Keys


class SjyglPage(WebKeys):
    # 新增数据源
    def input_search(self, words):
        self.locator("xpath", "//*[@id='app']/div/div[1]/div/div[2]/input").clear()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[1]/div/div[2]/input").send_keys(words)
        self.wait(1)

    def local_datasource(self):
        self.locator("xpath", "/html/body/div/div/div[1]/div/div[3]/div[1]/div/div/span").click()
        self.wait(1)

    def opera_datasource(self):
        self.locator("xpath", "//*[@id='app']/div/div[1]/div/div[3]/div[1]/div/div/div/span").click()
        self.wait(3)

    def add_datasource(self):
        self.locator("xpath", "/html/body/ul/li[1]").click()
        self.wait(1)

    def locator_database(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div/div/div/div[2]/div[6]/div[1]/div/span").click()
        self.wait(1)

# 数据源分组
    def icon_addcls(self):
        self.locator("xpath", "//*[@id='app']/div/div[1]/div/div[1]/span/span/a/i").click()
        self.wait(1)

    def edit_cls(self):
        self.locator("xpath", "/html/body/ul/li[2]").click()
        self.wait(1)

    def remove_cls(self):
        self.locator("xpath", "/html/body/ul/li[3]").click()
        self.wait(1)

    def input_clsname(self, words):
        self.locator("xpath", "/html/body/div[2]/div[1]/form/div/div/div[1]/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "/html/body/div[2]/div[1]/div/button[2]").click()
        self.wait(2)

    def input_editcls(self, words):
        self.locator("xpath", "//*[@id='app']/div/div[4]/div/div[2]/form/div/div[2]/div/input").send_keys(words)
        self.wait(1)

    def button_savecls(self):
        self.locator("xpath", "//*[@id='app']/div/div[4]/div/div[3]/span/button[2]").click()
        self.wait(1)

    def add_oracle(self, dataname, dataid, host, port, username, password):
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/span[1]").click()
        self.wait(1)
        self.locator("xpath","//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[1]/div/div/label[1]/span[1]/span").click()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[2]/div/div[1]/input").send_keys(dataname)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[3]/div/div[1]/input").send_keys(dataid)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[4]/div/div/input").send_keys(host)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[5]/div/div/input").clear()
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[5]/div/div/input").send_keys(port)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[6]/div/div[1]/input").send_keys(username)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[7]/div/div[1]/input").send_keys(password)
        self.wait(1)

    def add_hive(self, dataname_hive, dataid_hive, host_hive, port_hive, url_hive):
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/span[2]").click()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[1]/div/div/label[2]").click()
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[2]/div/div[1]/input").send_keys(dataname_hive)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[3]/div/div[1]/input").send_keys(dataid_hive)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[4]/div/div[1]/input").send_keys(host_hive)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[5]/div/div[1]/input").send_keys(port_hive)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[10]/div/div[1]/input").clear()
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[2]/form/div[10]/div/div[1]/input").send_keys(url_hive)
        self.wait(1)

    def test_join(self):
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[3]/button[1]").click()
        self.wait(5)

    def button_orasave(self):
        self.locator("xpath", "/html/body/div[3]/div/div[2]/div[2]/button").click()
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[3]/button[2]").click()
        self.wait(3)

    def button_hivesave(self):
        self.locator("xpath", "/html/body/div[3]/div/div[2]/div[2]/button").click()
        self.locator("xpath", "//*[@id='app']/div/div[3]/div/div/section/div/div[1]/div[3]/button[2]").click()
        self.wait(3)

    # 查看数据源
    def button_view(self):
        self.locator("xpath", "//*[@id='app']/div/div[2]/div/div[2]/div[1]/div[3]/span[3]").click()
        self.wait(1)

    def view_testjoin(self):
        self.locator("xpath", "//*[@id='app']/div/div[2]/div/div[2]/div[2]/div[2]/span[2]").click()
        self.wait(2)

    def view_switch(self):
        self.locator("xpath", "//*[@id='app']/div/div[2]/div/div[2]/div[2]/div[2]/span[4]").click()
        self.wait(2)

    def view_edit(self):
        self.locator("xpath", "//*[@id='app']/div/div[2]/div/div[2]/div[2]/div[2]/span[3]").click()
        self.wait(1)

    def view_back(self):
        self.locator("xpath", "//*[@id='app']/div/div[2]/div/div[1]/span[1]").click()
        self.wait(1)

    def view_cpback(self):
        self.locator("xpath", "//*[@id='app']/div/div[2]/div/div/div[1]/div[1]/span").click()
        self.wait(1)

    # 数据操作
    def data_todo(self):
        self.locator("xpath", "/html/body/div[1]/div/div[2]/div/div[2]/div[1]/div[1]/div/a/span").click()
        self.wait(1)

    def data_join(self):
        self.locator("xpath", "/html/body/ul[2]/li[1]").click()
        self.wait(1)

    def data_edit(self):
        self.locator("xpath", "/html/body/ul[2]/li[2]").click()
        self.wait(1)

    def data_copy(self):
        self.locator("xpath", "/html/body/ul[2]/li[3]").click()
        self.wait(2)

    def data_switch1(self):
        self.locator("xpath", "/html/body/ul[2]/li[4]").click()
        self.wait(1)

    def data_switch2(self):
        self.locator("xpath", "/html/body/ul/li[4]").click()
        self.wait(1)

    def data_remove1(self):
        self.locator("xpath", "/html/body/ul[2]/li[5]").click()
        self.wait(1)

    def data_remove2(self):
        self.locator("xpath", "/html/body/ul/li[5]").click()
        self.wait(1)