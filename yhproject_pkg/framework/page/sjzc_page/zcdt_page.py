# 数据门户
from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class ZcdtPage(WebKeys):
    def icon_sjdz(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[1]/div[1]").click()
        self.wait(2)

    def icon_ods(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[2]/div[1]").click()
        self.wait(1)

    def icon_jy(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[3]/div[1]").click()
        self.wait(1)

    def icon_standard(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[4]/div[2]").click()
        self.wait(1)

    def icon_zlfx(self):
        self.locator("xpath", "//*[@id='app']/section/div/div[1]/div[6]/div").click()
        self.wait(1)

    def tab_zcdt(self):
        self.locator("xpath", "//*[@id='tab-yhdgp:assets:map: dmptlmap']").click()
        self.wait(1)

    def icon_closed(self):
        self.locator("xpath", "/html/body/div/section/div/div[2]/div/div/header/button").click()
        self.wait(1)

