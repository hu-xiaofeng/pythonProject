# 数据门户
from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class SjmhPage(WebKeys):
    def input_keywords(self, words):
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div/div[2]/div/input").send_keys(words)
        self.wait(1)
        self.locator("xpath", "//*[@id='app']/section/div/div/div/div/div/div/div[2]/button").click()

    def button_meta(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div[1]/div[2]/div[2]").click()
        self.wait(1)

    def button_standard(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div[1]/div[2]/div[3]").click()
        self.wait(1)

    def button_classfiy(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div[1]/div[2]/div[4]").click()
        self.wait(1)

    def button_level(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div[1]/div[2]/div[5]").click()
        self.wait(1)

    def button_api(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div[1]/div[2]/div[6]").click()
        self.wait(1)

    def tab_zcmh(self):
        self.locator("xpath", "//*[@id='tab-yhdgp:assets:portal:dmptlhome']").click()
        self.wait(1)

    # 定位第一数据
    def local_data(self):
        self.locator("xpath", "//*[@id='app']/section/div/div/div[2]/div[1]/p[1]").click()
        self.wait(5)


