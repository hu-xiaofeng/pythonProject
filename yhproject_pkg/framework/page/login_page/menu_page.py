# 从关键字文件导入基类
from yhproject_pkg.framework.key_word.keyword_web import WebKeys


class MenuPage(WebKeys):
    def table_basic(self):
        self.locator("xpath", "/html/body/section/div/span/ul/div/li[1]").click()
        self.wait(1)

    def table_manage(self):
        self.locator("xpath", "/html/body/section/div/span/ul/div/li[2]").click()
        self.wait(3)

    def menu_icon(self):
        self.locator("xpath", "/html/body/section/div/div[1]/div/i").click()
        self.wait(3)

    # test_sjkf
    def menu_sjkf(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/section/ul/li[1]/div/span").click()
        self.wait(3)

    def menu_sys(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/section/ul/li/div/span").click()
        self.wait(3)

    # 数据源管理
    def sjkf_cjzx_sjygl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div/dl[1]/dd[1]").click()
        self.wait(3)

    def sjgk_sjygl_iframe(self):
        self.switch_iframe("id", "yhdgp:dev:exploration:commonDs")
        self.wait(3)

    # 数据管控
    def menu_sjgk(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/section/ul/li[2]/div/span").click()
        self.wait(3)

    # 数据分类分级
    def sjgk_sjflfj_sjfl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[4]/dd[1]").click()
        self.wait(3)

    def sjgk_sjflfj_sjfj(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[4]/dd[2]").click()
        self.wait(3)

    def sjgk_sjfl_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:classification:dataclassify")
        self.wait(5)

    def sjgk_sjfj_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:classification:datalevel")
        self.wait(3)

    def sjgk_sjfjfl_fjflsz(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div/dl[4]/dd[3]").click()
        self.wait(2)

    def sjgk_fjflsz_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:classification:dataAuthManage")

    # 脱敏规则
    def sjgk_sjaq_tmgz(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[6]/dd[1]").click()
        self.wait(3)

    def sjgk_tmgz_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:/dataSafemgr:desensitizerule")
        self.wait(3)

    # 数据脱敏
    def sjgk_sjaq_sjtm(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[6]/dd[2]").click()
        self.wait(3)

    def sjgk_sjtm_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:/dataSafemgr:desensitizeruleconfig")
        self.wait(3)

    # 脱敏查看
    def sjgk_sjaq_tmck(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[6]/dd[3]").click()
        self.wait(3)

    def sjgk_tmck_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:/dataSafemgr:desensitizemanage")
        self.wait(3)

    # 数据脱敏历史变更记录
    def sjgk_sjaq_tmls(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[6]/dd[4]").click()
        self.wait(1)

    def sjgk_tmls_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:/dataSafemgr:deshistory")
        self.wait(3)

    # 数据权限管理
    def sjgk_sjaq_sjqx(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[6]/dd[5]").click()
        self.wait(1)

    def sjgk_sjqx_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:/dataSafemgr:datapermission")
        self.wait(3)

    # 数据权限查看
    def sjgk_sjaq_sjqxck(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[6]/dd[6]").click()
        self.wait(1)

    def sjgk_sjqxck_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:/dataSafemgr:perview")
        self.wait(3)

    # 数据权限历史变更
    def sjgk_sjaq_sjqxlsbg(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[6]/dd[7]").click()
        self.wait(1)

    def sjgk_sjqxlsbg_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:/dataSafemgr:perhistory")
        self.wait(3)

    # 规则库管理
    def sjgk_sjzl_gzkgl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[3]/dd[1]").click()
        self.wait(3)

    def sjgk_gzkgl_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataQuality:ruleBase")
        self.wait(3)

    # 数据质量规则管理
    def sjgk_sjzl_sjzlgzgl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[3]/dd[2]").click()
        self.wait(3)

    def sjgk_sjzlgzgl_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataQuality:dataRuleManage")
        self.wait(3)

    # 质量监控
    def sjgk_sjzl_zljk(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[3]/dd[3]").click()
        self.wait(1)

    def sjgk_zljk_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataQuality:qualityMonitor")
        self.wait(3)

    # 质量分析
    def sjgk_sjzl_zlfx(self):
        self.locator("xpath","/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[3]/dd[4]").click()
        self.wait(1)

    def sjgk_zlfx_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataQuality:qualityAnalysis")
        self.wait(3)

    # 数据标准管理
    def sjgk_sjbz_sjbzgl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div/dl[2]/dd[1]").click()
        self.wait(3)

    def sjgk_sjbzgl_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:standard:standardManage")
        self.wait(3)

    # 落标监控
    def sjgk_sjbz_lbjk(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div/dl[2]/dd[2]").click()
        self.wait(3)

    def sjgk_lbjk_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:standard:fallMonitor")
        self.wait(3)

    # 标准覆盖
    def sjgk_sjbz_bzfg(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[2]/dd[2]").click()
        self.wait(3)

    def sjgk_bzfg_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:standard:standardCover")
        self.wait(3)

    # 数据服务
    def menu_sjfw(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/section/ul/li[6]/div/span").click()
        self.wait(3)

    # test_sjkf-采集中心-调度管理
    def sjkf_cjzx_ddgl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div/dl[1]/dd[4]").click()
        self.wait(3)

    def sjkf_ddgl_iframe(self):
        self.switch_iframe("id", "yhdgp:dev:collection:diaodu")
        self.wait(3)

    ####################################################################################################
    # test_sjkf-元数据-元数据管理
    def sjgk_ysj_ysjgl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[1]/dd[1]").click()
        self.wait(5)

    def sjgk_ysjgl_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:metaData:metaDataManage")
        self.wait(3)

    # test_sjkf-元数据-词根库管理
    def sjgk_ysj_cgkgl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[1]/dd[2]").click()
        self.wait(3)

    def sjgk_cgkgl_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:metaData:etyma")
        self.wait(3)

    # test_sjkf-元数据-参数字典管理
    def sjgk_ysj_cszd(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[1]/dd[3]/span[2]").click()
        self.wait(1)

    def sjgk_cszd_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:metaData:paramdict")
        self.wait(3)

    def sjgk_ysj_mjgl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[1]/dd[4]").click()
        self.wait(3)

    def sjgk_mjgl_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:metaData:metadict")
        self.wait(3)

    # 数据服务-数据服务-业务主题管理
    def sjfw_sjfw_ywztgl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[5]/dd[1]").click()
        self.wait(3)

    def sjfw_yeztgl_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataServer:apiTheme")
        self.wait(3)

    # 数据服务-数据服务-接口创建
    def sjfw_sjfw_jkcj(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[5]/dd[2]").click()
        self.wait(3)

    def sjfw_jkcj_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataServer:apiAdd")
        self.wait(3)

    # 数据服务-数据服务-接口审批
    def sjfw_sjfw_jksp(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[5]/dd[3]").click()
        self.wait(3)

    def sjfw_jksp_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataServer:apiReview")
        self.wait(3)

    # 数据服务-数据服务-接口上线管理
    def sjfw_sjfw_jksxgl(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[5]/dd[4]").click()
        self.wait(3)

    def sjfw_jksxgl_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataServer:apiOnline")
        self.wait(3)

    # 数据服务-数据服务-接口查询
    def sjfw_sjfw_jkcx(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[5]/dd[5]").click()
        self.wait(3)

    def sjfw_jkcx_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataServer:apiList")
        self.wait(3)

    # 角色管理
    def sys_role(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[1]/dd[2]").click()
        self.wait(1)

    def role_iframe(self):
        self.switch_iframe("id", "ycmp:sys:basic:role")
        self.wait(3)

    # 数据服务-数据服务-接口日志
    def sjfw_sjfw_jkrz(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[5]/dd[6]").click()
        self.wait(1)

    def sjfw_jkrz_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataServer:apilog")
        self.wait(1)

    # 数据服务-数据服务-权限查看
    def sjfw_sjfw_qxck(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[5]/dd[7]").click()
        self.wait(1)

    def sjfw_qxck_iframe(self):
        self.switch_iframe("id", "yhdgp:controll:dataServer:apipowervi")
        self.wait(1)

    # 数据开发-数据探源-探源分析
    def sjkf_sjty_tyfx(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl/dd[2]").click()
        self.wait(1)

    def sjkf_tyfx_iframe(self):
        self.switch_iframe("id", "yhdgp:dev:exploration:dataexplorration")
        self.wait(1)

    def sjkf_tyfx_iframe1(self):
        self.switch_iframe("id", "yhdgp:controll:exploration:dataexploration")
        self.wait(1)

    def sjkf_sjty_tyfh(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl/dd[3]").click()
        self.wait(1)

    def sjkf_tyfh_iframe(self):
        self.switch_iframe("id", "yhdgp:dev:exploration:explorationexamine")
        self.wait(1)

    def sjkf_sjty_tyjk(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl/dd[4]").click()
        self.wait(1)

    def sjkf_tyjk_iframe(self):
        self.switch_iframe("id", "yhdgp:dev:exploration:explorationmonitor")
        self.wait(1)

    # 数据资产
    def menu_sjzc(self):
        self.locator("xpath","/html/body/section/div/section[2]/section/section/ul/li[4]").click()
        self.wait(1)

    def sjzc_sjmh_sjmh(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[1]/dd").click()
        self.wait(1)

    def sjzc_sjmh_iframe(self):
        self.switch_iframe("id", "yhdgp:assets:portal:dmptlhome")
        self.wait(1)

    # 资产地图
    def sjzc_zcdt_zcdt(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[2]/dd").click()
        self.wait(1)

    def sjzc_zcdt_iframe(self):
        self.switch_iframe("id", "yhdgp:assets:map: dmptlmap")
        self.wait(1)

    # 资产导航
    def sjzc_zcdh_meta(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[3]/dd[1]").click()
        self.wait(1)

    def sjzc_meta_iframe(self):
        self.switch_iframe("id", "yhdgp:assets:navigatio:dmptldict")
        self.wait(1)

    def sjzc_zcdh_classify(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[3]/dd[2]").click()
        self.wait(1)

    def sjzc_classify_iframe(self):
        self.switch_iframe("id", "yhdgp:assets:navigatio:dmptlclass")
        self.wait(1)

    def sjzc_zcdh_level(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[3]/dd[3]").click()
        self.wait(1)

    def sjzc_level_iframe(self):
        self.switch_iframe("id", "yhdgp:assets:navigatio:dmptllevel")
        self.wait(1)

    def sjzc_zcdh_standard(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[3]/dd[4]").click()
        self.wait(1)

    def sjzc_standard_iframe(self):
        self.switch_iframe("id", "yhdgp:assets:navigatio: dmptlstan")
        self.wait(1)

    def sjzc_zcdh_api(self):
        self.locator("xpath", "/html/body/section/div/section[2]/section/div[1]/section/div/div/div[2]/div[1]/dl[3]/dd[5]").click()
        self.wait(1)

    def sjzc_api_iframe(self):
        self.switch_iframe("id", "yhdgp:assets:navigatio:dmptlapi")
        self.wait(1)