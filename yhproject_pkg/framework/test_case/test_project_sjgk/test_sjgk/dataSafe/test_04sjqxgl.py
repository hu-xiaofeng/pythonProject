import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.sjqx_page import SjqxMetaPage
from yhproject_pkg.framework.page.sjgk_page.sjqx_page import SjqxServicePage
from yhproject_pkg.framework.page.sjgk_page.sjqx_page import SjqxPage
import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y/%m/%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('数据权限管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入数据脱敏查看模块-------------')


@allure.feature('数据权限管理')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjaq_sjqx()
    # 切换iframe
    page.sjgk_sjqx_iframe()
    page.assert_att("数据权限管理")
    logger.info('01进入菜单成功')


@allure.feature('数据权限管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('tab_元数据')
def test_tab_meta(browser, data):
    page = SjqxMetaPage(browser)
    page.input_tables(data["database"])
    page.local_database()
    page.input_tables(data["tables"])
    page.local_tables()
    page.assert_att("表英文名")


@allure.feature('数据权限管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('搜索字段')
def test_query_fields(browser, data):
    page = SjqxPage(browser)
    page.input_fields(data["fields"])
    page.assert_att(data["fields"])


@allure.feature('数据权限管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('搜索字段')
def test_eidt_fields(browser, data):
    page = SjqxPage(browser)
    page.button_edit()
    page.button_sqlx()
    page.checkbox_role()
    page.button_save()
    page.assert_att("保存成功")


@allure.feature('数据权限管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('tab_数据服务')
def test_tab_service(browser, data):
    page = SjqxServicePage(browser)
    page.tab_datasever()
    page.input_tables(data["tables1"])
    page.local_tables()
    page.tab_datarange()
    page.input_fields(data["fields1"])
    page.icon_role()
    page.button_edit()
    page.button_compare()
    page.button_save()
    page.assert_att("保存成功")


@allure.feature('数据脱敏查看')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
