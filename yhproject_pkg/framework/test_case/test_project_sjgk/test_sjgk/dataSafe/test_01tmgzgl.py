import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.tmgz_page import TmgzPage
import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y%m%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('脱敏规则管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入脱敏规则管理模块-------------')


@allure.feature('脱敏规则管理')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjaq_tmgz()
    # 切换iframe
    page.sjgk_tmgz_iframe()
    page.assert_att("规则名称")
    logger.info('01进入菜单成功')


@allure.feature('脱敏规则管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/tmgz.yaml'))
@allure.title('新增规则')
def test_add_rule(browser, data):
    page = TmgzPage(browser)
    page.button_addrule()
    page.input_rulename(data["rulename"])
    page.input_expression(data["start_index"], data["end_index"])
    page.input_replace(data["replace"])
    page.button_save()
    page.assert_att("操作成功")


@allure.feature('脱敏规则管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/tmgz.yaml'))
@allure.title('查询规则')
def test_search_rule(browser, data):
    page = TmgzPage(browser)
    page.input_searchrule(data["rulename"])
    page.assert_att(data["rulename"])


@allure.feature('脱敏规则管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/tmgz.yaml'))
@allure.title('编辑规则')
def test_edit_rule(browser, data):
    page = TmgzPage(browser)
    page.button_edit()
    page.input_editrule("remark")
    page.button_editsave()
    page.assert_att("保存成功")


@allure.feature('脱敏规则管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/tmgz.yaml'))
@allure.title('编辑后查询规则')
def test_search_rule(browser, data):
    page = TmgzPage(browser)
    page.button_reset()
    page.input_searchrule(data["rulename"]+"remark")
    page.assert_att(data["rulename"]+"remark")


@allure.feature('脱敏规则管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/tmgz.yaml'))
@allure.title('停启用规则')
def test_stop_rule(browser, data):
    page = TmgzPage(browser)
    page.button_stop()
    page.enter_keys()
    page.assert_att("启用规则")
    page.button_stop()
    page.enter_keys()
    page.assert_att("停用规则")


@allure.feature('脱敏规则管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/tmgz.yaml'))
@allure.title('停启用推荐')
def test_recommand_rule(browser, data):
    page = TmgzPage(browser)
    page.button_recommand()
    page.enter_keys()
    page.assert_att("启用推荐")
    page.button_recommand()
    page.enter_keys()
    page.assert_att("停用推荐")


@allure.feature('脱敏规则管理')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
