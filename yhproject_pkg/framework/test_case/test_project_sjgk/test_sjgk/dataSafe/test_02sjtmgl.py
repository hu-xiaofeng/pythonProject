import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.sjtm_page import SjtmMetaPage
from yhproject_pkg.framework.page.sjgk_page.sjtm_page import SjtmServicePage
from yhproject_pkg.framework.page.sjgk_page.sjtm_page import SjtmPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('数据脱敏')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入数据脱敏模块-------------')


@allure.feature('数据脱敏')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjaq_sjtm()
    # 切换iframe
    page.sjgk_sjtm_iframe()
    page.assert_att("数据脱敏配置")
    logger.info('01进入菜单成功')


@allure.feature('数据脱敏')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('tab_元数据')
def test_tab_meta(browser, data):
    page = SjtmMetaPage(browser)
    page.input_tables(data["database"])
    page.local_database()
    page.input_tables(data["tables"])
    page.local_tables()
    page.assert_att("表英文名")


@allure.feature('数据脱敏')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('搜索字段信息')
def test_query_metafields(browser, data):
    page = SjtmPage(browser)
    page.input_fields(data["fields"])
    page.assert_att(data["fields"])


@allure.feature('数据脱敏')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('编辑规则')
def test_add_metarule(browser, data):
    page = SjtmPage(browser)
    page.button_editrule()
    page.icon_addrule()
    page.input_role(data["role"])
    page.input_rule(data["rulename"])
    page.button_addrule()
    page.button_save()
    page.assert_att("普")
    page.icon_closed()
    page.assert_att("1")


@allure.feature('数据脱敏')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('tab_数据服务')
def test_tab_service(browser, data):
    page = SjtmServicePage(browser)
    page.tab_datasever()
    page.input_tables(data["tables1"])
    page.local_tables()
    page.assert_att("接口名")


@allure.feature('数据脱敏')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('搜索参数信息')
def test_query_servicefields(browser, data):
    page = SjtmPage(browser)
    page.input_fields(data["fields1"])
    page.assert_att(data["fields1"])


@allure.feature('数据脱敏')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('编辑规则')
def test_add_servicerule(browser, data):
    page = SjtmPage(browser)
    page.button_editserverule()
    page.icon_addrule()
    page.input_role(data["role"])
    page.input_rule(data["rulename"])
    page.button_addrule()
    page.button_save()
    page.assert_att("普")
    page.icon_closed()
    page.assert_att("1")


@allure.feature('数据脱敏')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('移除规则')
def test_remove_servicerule(browser, data):
    page = SjtmPage(browser)
    page.button_editserverule()
    page.icon_removerule()
    page.assert_att("已删除")
    page.icon_closed()
    page.assert_att("0")


@allure.feature('数据脱敏')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
