import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.tmck_page import TmckPage
from yhproject_pkg.framework.page.sjgk_page.tmck_page import TmlsPage
from yhproject_pkg.framework.page.sjgk_page.tmgz_page import TmgzPage
import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y/%m/%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('数据脱敏查看')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入数据脱敏查看模块-------------')


@allure.feature('数据脱敏查看')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjaq_tmck()
    # 切换iframe
    page.sjgk_tmck_iframe()
    page.assert_att("约束对象")
    logger.info('01进入菜单成功')


@allure.feature('数据脱敏查看')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('搜索脱敏')
def test_search_data(browser, data):
    page = TmckPage(browser)
    page.input_tables(data["tables"])
    page.input_fields(data["fields"])
    page.button_search()
    page.assert_att(data["tables"])


@allure.feature('数据脱敏查看')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('停用脱敏')
def test_stop_data(browser, data):
    page = TmckPage(browser)
    page.button_stop()
    page.enter_keys()
    page.assert_att("操作成功")
    page.reback_iframe()


@allure.feature('数据脱敏查看')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('进入数据脱敏历史变更记录')
def test_menu_sjtmls(browser, data):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjaq_tmls()
    # 切换iframe
    page.sjgk_tmls_iframe()
    page.assert_att("有效期限")
    logger.info('01进入脱敏历史菜单成功')


@allure.feature('数据脱敏查看')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('搜索脱敏历史')
def test_search_history(browser, data):
    page = TmlsPage(browser)
    page.input_tables(data["tables"])
    page.input_fields(data["fields"])
    page.button_search()
    page.assert_att("停用")
    page.reback_iframe()


@allure.feature('数据脱敏查看')
@allure.title('启用脱敏规则')
def test_open_tmck(browser):
    page = TmckPage(browser)
    page1 = MenuPage(browser)
    page.tab_tmck()
    page1.sjgk_tmck_iframe()
    page.button_search()
    page.button_stop()
    page.enter_keys()
    page.assert_att("操作成功")
    page.reback_iframe()


@allure.feature('数据脱敏查看')
@allure.title('检查脱敏变更')
def test_check_history(browser):
    page = TmlsPage(browser)
    page1 = MenuPage(browser)
    page.tab_tmls()
    page1.sjgk_tmls_iframe()
    page.button_search()
    page.assert_att("启用")
    page.reback_iframe()


@allure.feature('数据脱敏查看')
@allure.title('删除脱敏配置')
def test_remove_config(browser):
    page = TmckPage(browser)
    page1 = MenuPage(browser)
    page.tab_tmck()
    page1.sjgk_tmck_iframe()
    page.button_remove()
    page.enter_keys()
    page.assert_att("已删除")
    page.reback_iframe()


@allure.feature('数据脱敏查看')
@allure.title('再次检查脱敏变更')
def test_check2_history(browser):
    page = TmlsPage(browser)
    page1 = MenuPage(browser)
    page.tab_tmls()
    page1.sjgk_tmls_iframe()
    page.button_search()
    page.assert_att("停用")
    page.assert_att(now1)
    page.reback_iframe()


@allure.feature('数据脱敏查看')
@allure.title('进入脱敏规则管理')
def test_switch_tmgzgl(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjaq_tmgz()
    # 切换iframe
    page.sjgk_tmgz_iframe()
    page.assert_att("规则名称")
    logger.info('01进入脱敏规则菜单成功')


@allure.feature('数据脱敏查看')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/tmgz.yaml'))
@allure.title('删除规则')
def test_search_rule(browser, data):
    page = TmgzPage(browser)
    page.input_searchrule(data["rulename"])
    page.assert_att(data["rulename"])
    page.button_remove()
    page.enter_keys()
    page.assert_att("已删除")


@allure.feature('数据脱敏查看')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')