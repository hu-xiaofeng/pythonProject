import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.sjqxck_page import SjckPage
from yhproject_pkg.framework.page.sjgk_page.sjqxck_page import SjlsPage


import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y/%m/%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('数据权限查看')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入数据脱敏查看模块-------------')


@allure.feature('数据权限查看')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjaq_sjqxck()
    # 切换iframe
    page.sjgk_sjqxck_iframe()
    page.assert_att("约束对象")
    logger.info('01进入菜单成功')


@allure.feature('数据权限查看')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('搜索数据')
def test_search_data(browser, data):
    page = SjckPage(browser)
    page.input_tables(data["tables"])
    page.input_fields(data["fields"])
    page.button_search()
    page.assert_att(data["tables"])


@allure.feature('数据权限查看')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('停用数据权限')
def test_stop_data(browser, data):
    page = SjckPage(browser)
    page.button_stop()
    page.enter_keys()
    page.assert_att("停用")
    page.reback_iframe()


@allure.feature('数据权限查看')
@allure.title('进入数据脱敏历史变更记录')
def test_menu_sjtmls(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjaq_sjqxlsbg()
    # 切换iframe
    page.sjgk_sjqxlsbg_iframe()
    page.assert_att("有效期限")
    logger.info('01进入脱敏历史菜单成功')


@allure.feature('数据权限查看')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjtm.yaml'))
@allure.title('搜索数据权限历史')
def test_search_history(browser, data):
    page = SjlsPage(browser)
    page.input_tables(data["tables"])
    page.input_fields(data["fields"])
    page.button_search()
    page.assert_att("停用")
    page.reback_iframe()


@allure.feature('数据权限查看')
@allure.title('启用数据权限')
def test_open_sjck(browser):
    page = SjckPage(browser)
    page1 = MenuPage(browser)
    page.tab_sjck()
    page1.sjgk_sjqxck_iframe()
    page.button_search()
    page.button_stop()
    page.enter_keys()
    page.assert_att("启用")
    page.reback_iframe()


@allure.feature('数据权限查看')
@allure.title('检查脱敏变更')
def test_check_history(browser):
    page = SjlsPage(browser)
    page1 = MenuPage(browser)
    page.tab_sjls()
    page1.sjgk_sjqxlsbg_iframe()
    page.button_search()
    page.assert_att("启用")
    page.reback_iframe()


@allure.feature('数据权限查看')
@allure.title('删除脱敏配置')
def test_remove_config(browser):
    page = SjckPage(browser)
    page1 = MenuPage(browser)
    page.tab_sjck()
    page1.sjgk_sjqxck_iframe()
    page.button_remove()
    page.enter_keys()
    page.assert_att("已删除")
    page.reback_iframe()


@allure.feature('数据权限查看')
@allure.title('再次检查数据权限')
def test_check2_history(browser):
    page = SjlsPage(browser)
    page1 = MenuPage(browser)
    page.tab_sjls()
    page1.sjgk_sjqxlsbg_iframe()
    page.button_search()
    page.assert_att("停用")
    page.assert_att(now1)
    page.reback_iframe()


@allure.feature('数据脱敏查看')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')

