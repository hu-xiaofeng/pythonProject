import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.zlgzrwgl_page import ZlgzrwPage
import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y%m%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('质量规则任务管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入质量规则任务管理模块-------------')


@allure.feature('质量规则任务管理')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjzl_sjzlgzgl()
    # 切换iframe
    page.sjgk_sjzlgzgl_iframe()
    page.assert_att("前往任务列表")
    logger.info('01进入菜单成功')


@allure.feature('质量规则任务管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/gzkgl.yaml'))
@allure.title('新建任务')
def test_addtask(browser, data):
    page = ZlgzrwPage(browser)
    page.button_addtask()
    page.input_checkcode(data["checkcode"])
    page.down_keys()
    page.enter_keys()
    page.input_taskname("任务"+now1)
    page.button_addcomfirm()
    page.assert_att("操作成功")


@allure.feature('质量规则任务管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/gzkgl.yaml'))
@allure.title('查询任务')
def test_querytask(browser, data):
    page = ZlgzrwPage(browser)
    page.input_keycode(data["checkcode"])
    page.assert_att("任务"+now1)


@allure.feature('质量规则任务管理')
@allure.title('执行任务')
def test_actiontask(browser):
    page = ZlgzrwPage(browser)
    page.button_action()
    page.enter_keys()
    page.wait(3)
    page.assert_att("操作成功")


@allure.feature('质量规则任务管理')
@allure.title('查看日志')
def test_viewlog(browser):
    page = ZlgzrwPage(browser)
    page.button_log()
    page.assert_att("成功")
    page.icon_closelog()


@allure.feature('质量规则任务管理')
@allure.title('编辑入口')
def test_edittask(browser):
    page = ZlgzrwPage(browser)
    page.button_edit()
    page.assert_att("请先停用该规则任务")
    page.button_switch()
    page.enter_keys()
    page.assert_att("操作成功")
    page.button_edit()
    page.assert_att("修改任务配置信息")


@allure.feature('质量规则任务管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/gzkgl.yaml'))
@allure.title('编辑')
def test_edittask1(browser, data):
    page = ZlgzrwPage(browser)
    page.input_taskeidt("remark")
    page.assert_att("操作成功")
    page.wait(2)
    page.button_reset()
    page.input_keycode(data["checkcode"])
    page.assert_att("任务"+now1+"remark")


@allure.feature('质量规则任务管理')
@allure.title('删除')
def test_remove(browser):
    page = ZlgzrwPage(browser)
    page.button_remove()
    page.enter_keys()
    page.assert_att("暂无数据")


@allure.feature('质量规则任务管理')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
