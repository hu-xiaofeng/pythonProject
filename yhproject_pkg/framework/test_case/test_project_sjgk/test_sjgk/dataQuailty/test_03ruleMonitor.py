import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.zljk_page import ZljkPage
import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y-%m-%d")
now2 = now.strftime("%Y%m%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('质量监控')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入质量监控模块-------------')


@allure.feature('质量监控')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjzl_zljk()
    # 切换iframe
    page.sjgk_zljk_iframe()
    page.assert_att("检查规则数")
    logger.info('01进入菜单成功')


@allure.feature('质量监控')
@allure.title('查看检查失败')
def test_checkfail(browser):
    page = ZljkPage(browser)
    page.button_fail()
    page.assert_att("检查失败明细")
    page.icon_failclosed()


@allure.feature('质量监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/zljk.yaml'))
@allure.title('检查编码搜索')
def test_checkcode(browser, data):
    page = ZljkPage(browser)
    page.input_rulecode(data["rulecode_error"])
    page.assert_att(data["rulecode_error"])


@allure.feature('质量监控')
@allure.title('查看异常明细数据')
def test_checklist(browser):
    page = ZljkPage(browser)
    page.button_view()
    page.assert_att("异常数据明细")
    page.button_viewclosed()


@allure.feature('质量监控')
@allure.title('分析异常明细数据')
def test_analysis(browser):
    page = ZljkPage(browser)
    page.button_analysis()
    page.assert_att("分析")
    page.button_analysisclosed()


@allure.feature('质量监控')
@allure.title('跟踪异常明细数据')
def test_viewerror(browser):
    page = ZljkPage(browser)
    page.button_track()
    page.assert_att("异常规则跟踪记录")


@allure.feature('质量监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/zljk.yaml'))
@allure.title('记录异常明细数据')
def test_remarkerror(browser, data):
    page = ZljkPage(browser)
    page.input_notice()
    page.input_name(data["name"])
    page.input_textarea(data["textarea"])
    page.button_remarkcomfirm()
    page.wait(2)
    page.assert_att("操作成功")
    page.button_remarkback()


@allure.feature('质量监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/zljk.yaml'))
@allure.title('查看历史明细')
def test_history(browser, data):
    page = ZljkPage(browser)
    page.input_rulecode(data["rulecode_succes"])
    page.icon_history()
    page.icon_startdate(now1)
    page.icon_enddate(now1)
    page.button_search()
    page.assert_att(now2)


@allure.feature('质量监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/zljk.yaml'))
@allure.title('查看正常')
def test_checksucess(browser, data):
    page = ZljkPage(browser)
    page.icon_history()
    page.button_reset()
    page.input_rulecode(data["rulecode_succes"])
    page.assert_att("正常")


@allure.feature('质量监控')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')


