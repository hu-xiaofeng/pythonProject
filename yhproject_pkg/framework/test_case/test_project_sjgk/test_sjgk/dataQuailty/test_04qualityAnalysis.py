import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.zlfx_page import ZlfxPage
import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y-%m-%d")
now2 = now.strftime("%Y%m%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('质量分析')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入质量分析模块-------------')


@allure.feature('质量分析')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjzl_zlfx()
    # 切换iframe
    page.sjgk_zlfx_iframe()
    page.assert_att("检查日期")
    logger.info('01进入菜单成功')


@allure.feature('质量分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/zlfx.yaml'))
@allure.title('通过日期检查下钻')
def test_list_date(browser, data):
    page = ZlfxPage(browser)
    page.input_startdate(str(data["startdate"]))
    page.input_enddate(str(data["enddate"]))
    page.button_search()
    page.date_query()
    page.wait(2)
    page.date_query()
    page.wait(2)
    page.assert_att("查看检查失败")
    page.button_rebackanalysis()
    page.wait(2)


@allure.feature('质量分析')
@allure.title('查看明细-数据项')
def test_check_data(browser):
    page = ZlfxPage(browser)
    page.button_dataview()
    page.assert_att("检查对象-数据项")
    page.button_dataclosed()


@allure.feature('质量分析')
@allure.title('查看明细-字段')
def test_check_field(browser):
    page = ZlfxPage(browser)
    page.button_fieldview()
    page.assert_att("检查对象-字段")
    page.button_dataclosed()


@allure.feature('质量分析')
@allure.title('查看明细-表')
def test_check_table(browser):
    page = ZlfxPage(browser)
    page.button_tableview()
    page.assert_att("检查对象-表")
    page.button_dataclosed()


@allure.feature('质量分析')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')







