import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.gzkgl_page import GzkglPage
import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y%m%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('规则库管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入规则库管理模块-------------')


@allure.feature('规则库管理')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjzl_gzkgl()
    # 切换iframe
    page.sjgk_gzkgl_iframe()
    page.assert_att("检查规则名称")
    logger.info('01进入菜单成功')


@allure.feature('规则库管理')
@allure.title('规则库高级查询')
def test_high_query(browser):
    page = GzkglPage(browser)
    page.button_highquery()
    page.box_query()
    page.assert_att("CHK000308")


@allure.feature('规则库管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/gzkgl.yaml'))
@allure.title('规则库精准查询')
def test_keywords_query(browser, data):
    page = GzkglPage(browser)
    page.button_reset()
    page.input_checkcode(data["checkcode"])
    page.assert_att(data["checkcode"])


@allure.feature('规则库管理')
@allure.title('编辑')
def test_edit(browser):
    page = GzkglPage(browser)
    page.button_eidt()
    page.input_rulename("测试"+now1)
    page.scroll_top()
    page.button_editsave()
    page.assert_att("操作成功")


@allure.feature('规则库管理')
@allure.title('查看详情')
def test_view(browser):
    page = GzkglPage(browser)
    page.button_view()
    page.assert_att("测试"+now1)
    page.scroll_top()
    page.button_reback()
    page.assert_att("操作成功")


@allure.feature('规则库管理')
@allure.title('停用启用')
def test_switch(browser):
    page = GzkglPage(browser)
    page.button_switch()
    page.enter_keys()
    page.assert_att("有效")
    page.button_switch()
    page.enter_keys()
    page.assert_att("操作成功")
    page.assert_att("停用")


@allure.feature('规则库管理')
@allure.title('删除')
def test_remove(browser):
    page = GzkglPage(browser)
    page.box_check()
    page.button_remove()
    page.assert_att("有效状态不能删除")


@allure.feature('规则库管理')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
