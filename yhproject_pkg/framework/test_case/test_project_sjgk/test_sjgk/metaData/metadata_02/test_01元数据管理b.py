import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.ysjgl_page import YsjglPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('元数据管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_ysjgl(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入元数据管理模块-------------')


@allure.feature('元数据管理')
@allure.title('打开菜单')
def test_menu_ysjgl(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_ysj_ysjgl()
    # 切换iframe
    page.sjgk_ysjgl_iframe()
    page.assert_att("元数据库")
    logger.info('打开菜单成功')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('展开表字段')
def test_checkfield(browser, data):
    page = YsjglPage(browser)
    page.input_searchcls(data["clsname"])
    page.view_table(data["tablename"], data["fieldname"])
    page.view_field()
    logger.info('展开元数据表的字段')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('清除枚举值')
def test_clearmenu(browser, data):
    page = YsjglPage(browser)
    page.button_editemun()
    page.input_dictcode(data["dictcode"])
    page.checkbox_dictcode()
    page.button_dictcomfirm()
    logger.info('清除枚举值信息')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('清除数据项')
def test_clearfieldcls(browser, data):
    page = YsjglPage(browser)
    page.button_editfield()
    page.edit_fieldcls()
    page.input_datacode(data["datacode"])
    page.checkbox_datacode()
    page.button_tableclssave()
    page.button_fieldsave()
    logger.info('清除数据项成功')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('清除字段注释')
def test_fieldremark(browser, data):
    page =YsjglPage(browser)
    page.button_editfield()
    page.input_fieldremark(" ")
    page.button_fieldsave()
    logger.info('清除字段注释')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('清除表分类')
def test_cleartablecls(browser, data):
    page = YsjglPage(browser)
    page.edit_table()
    page.button_tablecls()
    page.input_tableclsname(data["tableclsname"])
    page.check_tableclsname()
    page.button_tableclssave()
    page.button_edsave()
    logger.info('清除表分类')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('清除表中文名')
def test_tableremark(browser, data):
    page = YsjglPage(browser)
    page.edit_table()
    page.clear_tablechiname()
    page.input_tablechiname(" ")
    page.button_edsave()
    logger.info('清除表中文名')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('编辑分组名称')
def test_editcls(browser, data):
    page = YsjglPage(browser)
    page.local_datacls()
    page.button_editdatacls()
    page.input_datacls("S")
    page.button_dataclscomfirm()
    page.assert_att("操作成功")
    logger.info('编辑分组名称成功')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('删除表分类')
def test_removecls(browser, data):
    page = YsjglPage(browser)
    page.input_searchcls(data["clsname"]+"S")
    page.local_datacls()
    page.button_removedatacls()
    page.enter_keys()
    logger.info('删除表分组成功')


@allure.feature('元数据管理')
@allure.title('退出登录')
def test_sjygl_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')


