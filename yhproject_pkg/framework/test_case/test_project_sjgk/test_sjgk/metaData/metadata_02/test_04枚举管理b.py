import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.mjgl_page import MjglPage

import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('枚举管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_mjgl(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入元数据管理模块-------------')


@allure.feature('枚举管理')
@allure.title('打开菜单_枚举管理')
def test_menu_mjgl(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_ysj_mjgl()
    # 切换iframe
    page.sjgk_mjgl_iframe()
    page.assert_att("字典列表")
    logger.info('打开菜单成功')


@allure.feature('枚举管理')
@allure.title('删除字典分组')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_removedictcls(browser, data):
    page = MjglPage(browser)
    page.input_search(data["staticCode"])
    page.local_dict()
    page.button_removecls()
    page.enter_keys()
    page.assert_att("已删除")
    page.input_search(data["staticCode1"])
    page.local_dict()
    page.button_removecls()
    page.enter_keys()
    page.assert_att("已删除")
    page.input_search(data["dynamicCode"])
    page.local_dict()
    page.button_removecls()
    page.enter_keys()
    page.assert_att("已删除")


@allure.feature('枚举管理')
@allure.title('退出登录')
def test_mjgl_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
