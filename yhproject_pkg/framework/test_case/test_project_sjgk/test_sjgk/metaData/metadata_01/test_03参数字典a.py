import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.cszd_page import CszdPage

import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('参数字典')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_cszd(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入参数字典模块-------------')


@allure.feature('参数字典')
@allure.title('打开菜单')
def test_menu_cszd(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_ysj_cszd()
    # 切换iframe
    page.sjgk_cszd_iframe()
    page.assert_att("参数字典")
    logger.info('打开菜单成功')


@allure.feature('参数字典')
@allure.title('新增参数字典')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/cszd.yaml'))
def test_adddict(browser, data):
    page = CszdPage(browser)
    page.button_addcszd()
    page.input_ysjbase(data["ysjbase"])
    page.down_keys()
    page.enter_keys()
    page.input_ysjtable(data["ysjtable"])
    page.button_search()
    page.button_checktable()
    page.button_comfirm()
    page.icon_closed()
    logger.info('新增参数字典')


@allure.feature('参数字典')
@allure.title('查询参数字典')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/cszd.yaml'))
def test_searchdict(browser, data):
    page = CszdPage(browser)
    page.input_search(data["ysjtable"])
    page.assert_att(data["ysjtable"])
    logger.info('查询参数字典')


@allure.feature('参数字典')
@allure.title('编辑参数字典')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/cszd.yaml'))
def test_editdict(browser, data):
    page = CszdPage(browser)
    page.local_paramsdict()
    page.button_edit()
    page.input_describe("remark")
    page.button_editsave()
    page.local_paramsdict()
    page.assert_att("remark")
    logger.info('编辑参数字典')


@allure.feature('参数字典')
@allure.title('退出登录')
def test_cszd_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')







