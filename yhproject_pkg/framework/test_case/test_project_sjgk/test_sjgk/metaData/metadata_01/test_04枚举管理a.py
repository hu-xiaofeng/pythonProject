import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.mjgl_page import MjglPage

import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('枚举管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_mjgl(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入元数据管理模块-------------')


@allure.feature('枚举管理')
@allure.title('打开菜单_枚举管理')
def test_menu_mjgl(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_ysj_mjgl()
    # 切换iframe
    page.sjgk_mjgl_iframe()
    page.assert_att("字典列表")
    logger.info('打开菜单成功')


@allure.feature('枚举管理')
@allure.title('新增单维字典分组')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_addstatic(browser, data):
    page = MjglPage(browser)
    page.button_addmjglcls()
    page.button_staticdict()
    page.input_staticdictcode(data["staticCode"])
    page.input_staticdictname(data["staticName"])
    page.input_staticremark(data["staticRemark"])
    page.button_addsave()
    page.assert_att("操作成功")
    logger.info('新增单维字典分组成功')


@allure.feature('枚举管理')
@allure.title('新增多维字典分组')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_addstatic1(browser, data):
    page = MjglPage(browser)
    page.button_addmjglcls()
    page.button_staticdict()
    page.button_multidict()
    page.input_staticdictcode(data["staticCode1"])
    page.input_staticdictname(data["staticName1"])
    page.input_staticremark(data["staticRemark1"])
    page.button_addsave()
    page.assert_att("操作成功")
    logger.info('新增层级字典分组成功')


@allure.feature('枚举管理')
@allure.title('新增动态字典分组')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_adddynamic(browser, data):
    page = MjglPage(browser)
    page.button_addmjglcls()
    page.button_dynamicdict()
    page.input_dynamicdictcode(data["dynamicCode"])
    page.input_dynamicdictname(data["dynamicName"])
    page.input_dynamicdictremark(data["dynamicRemark"])
    page.button_addsave()
    page.assert_att("操作成功")
    logger.info('新增动态字典分组成功')


@allure.feature('枚举管理')
@allure.title('定位单维字典')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_localdict(browser, data):
    page = MjglPage(browser)
    page.input_search(data["staticCode"])
    page.local_dict()
    logger.info('定位单维字典分组成功')


@allure.feature('枚举管理')
@allure.title('新增单维字典值')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_addstaticdict(browser, data):
    page = MjglPage(browser)
    page.button_addstaticdict()
    page.assert_att("新增子项")
    page.input_sindictcode(data["sinDictCode"])
    page.input_sindictvalue(data["sinDictValue"])
    page.input_sindictremark(data["sinDictRemark"])
    page.button_sindictsave()
    page.assert_att("操作成功")
    logger.info('新增单维字典值成功')


@allure.feature('枚举管理')
@allure.title('编辑单维字典值')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_editstaticdict(browser, data):
    page = MjglPage(browser)
    page.input_searchsindict(data["sinDictCode"])
    page.edit_sindict()
    page.input_editsindictcode(data["sinDictCode1"])
    page.input_eidtsindictvalue(data["sinDictValue1"])
    page.button_editsindictsave()
    page.assert_att("操作成功")
    logger.info('编辑单维字典值成功')


@allure.feature('枚举管理')
@allure.title('搜索单维字典值')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_searchstaticdict(browser, data):
    page = MjglPage(browser)
    page.input_searchsindict(data["sinDictCode"])
    page.assert_att("暂无数据")
    page.input_searchsindict(data["sinDictCode1"])
    page.assert_att(data["sinDictCode1"])
    logger.info('搜索单维字典值成功')


@allure.feature('枚举管理')
@allure.title('删除单维字典值')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_removestaticdict(browser, data):
    page = MjglPage(browser)
    page.remove_sindict()
    page.enter_keys()
    page.assert_att("已删除")
    logger.info('删除单维字典值成功')


# 新增层级枚举值
@allure.feature('枚举管理')
@allure.title('定位层级字典')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_localdict1(browser, data):
    page = MjglPage(browser)
    page.input_search(data["staticCode1"])
    page.local_dict()
    logger.info('定位层级字典成功')


@allure.feature('枚举管理')
@allure.title('新增层级字典值')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_addstaticdict1(browser, data):
    page = MjglPage(browser)
    page.button_addstaticdict()
    page.assert_att("新增子项")
    page.input_sindictcode(data["mutiDictCode"])
    page.input_sindictvalue(data["mutiDictValue"])
    page.input_sindictremark(data["mutiDictRemark"])
    page.button_sindictsave()
    page.assert_att("操作成功")
    logger.info('新增层级字典值成功')


@allure.feature('枚举管理')
@allure.title('新增子层级字典值')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_addstaticdict2(browser, data):
    page = MjglPage(browser)
    page.button_reset()
    page.button_minidict()
    page.input_minicode(data["minicode"])
    page.input_minivalue(data["minivalue"])
    page.input_miniremark(data["miniremark"])
    page.input_minisave()
    page.assert_att("操作成功")
    page.icon_expand()
    page.assert_att(data["minivalue"])
    logger.info('新增子层级字典值成功')


@allure.feature('枚举管理')
@allure.title('编辑子层级字典值')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_editstaticdict2(browser, data):
    page = MjglPage(browser)
    page.button_miniedit()
    page.input_minieditcode(data["minieditcode"])
    page.input_minieditvalue(data["minieditvalue"])
    page.button_minieditsave()
    page.icon_expand()
    page.assert_att(data["minieditvalue"])
    logger.info('编辑子层级字典值成功')


@allure.feature('枚举管理')
@allure.title('删除子层级字典值')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_removestaticdict2(browser, data):
    page = MjglPage(browser)
    page.button_miniremove()
    page.enter_keys()
    page.button_minifaremove()
    page.enter_keys()
    page.assert_att("暂无数据")
    logger.info('删除层级字典值成功')


# 新增动态字典
@allure.feature('枚举管理')
@allure.title('定位动态字典')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_localdict2(browser, data):
    page = MjglPage(browser)
    page.input_search(data["dynamicCode"])
    page.local_dict()
    logger.info('定位动态成功')


@allure.feature('枚举管理')
@allure.title('新增动态字典值')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_adddynamicdict(browser, data):
    page = MjglPage(browser)
    page.button_adddynamic()
    page.input_searchdynamic(data["dynamictable"])
    page.local_tablename()
    page.input_keys()
    page.input_value()
    page.input_conditionsql(data["sqlcondition"])
    page.button_viewdata()
    page.assert_att("暂无数据")
    logger.info('新增动态字典成功')


@allure.feature('枚举管理')
@allure.title('编辑动态字典值')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/mjgl.yaml'))
def test_editdynamicdict(browser, data):
    page = MjglPage(browser)
    page.input_conditionsql(data["sqlcondition1"])
    page.button_viewdata()
    page.assert_att("ROOT")
    page.button_savedynamci()
    page.button_reback()
    page.assert_att("字典详情")
    logger.info('修改动态字典值成功')


@allure.feature('枚举管理')
@allure.title('退出登录')
def test_mjgl_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
