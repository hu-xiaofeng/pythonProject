import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.cgk_page import CgkPage

import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('词根库管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_cgkgl(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入词根库管理模块-------------')


@allure.feature('词根库管理')
@allure.title('打开菜单')
def test_menu_cgkgl(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_ysj_cgkgl()
    # 切换iframe
    page.sjgk_cgkgl_iframe()
    page.assert_att("中文名称")
    logger.info('打开菜单成功')


@allure.feature('词根库管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/cgkgl.yaml'))
@allure.title('新增词根库')
def test_addcgk(browser, data):
    page = CgkPage(browser)
    page.button_addcgk()
    page.assert_att("新增")
    page.input_cgkchiname(data["cgkchiname"])
    page.input_cgkenglishname(data["cgkenglishname"])
    page.input_sort(data["cgksort"])
    page.button_cgksave()
    page.assert_att("操作成功")
    logger.info('新增词根库成功')


@allure.feature('词根库管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/cgkgl.yaml'))
@allure.title('搜索词根库')
def test_searccgk1(browser, data):
    page = CgkPage(browser)
    page.input_search(data["cgkchiname"])
    page.assert_att(data["cgkchiname"])
    logger.info('搜索词根库成功')


@allure.feature('词根库管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/cgkgl.yaml'))
@allure.title('修改词根库')
def test_editcgk(browser, data):
    page = CgkPage(browser)
    page.button_managecgk()
    page.button_edit()
    page.input_xlschinaname(data["xlschinaname"])
    page.input_xlsenglishname(data["xlsenglishname"])
    page.input_xlssort(data["xlssort"])
    page.button_comfirm()
    logger.info('修改词根库成功')


@allure.feature('词根库管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/cgkgl.yaml'))
@allure.title('修改搜索后的词根库')
def test_searccgk2(browser, data):
    page = CgkPage(browser)
    page.input_search(data["cgkchiname"])
    page.assert_att("暂无数据")
    page.input_search(data["xlschinaname"])
    page.assert_att(data["xlschinaname"])
    logger.info('搜索修改后的词根库成功')


@allure.feature('词根库管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/cgkgl.yaml'))
@allure.title('关闭管理')
def test_cgkgl(browser, data):
    page =CgkPage(browser)
    page.button_closemanage()
    page.assert_att("管理")
    logger.info('关闭管理成功')


@allure.feature('词根库管理')
@allure.title('退出登录')
def test_cgkgl_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')





