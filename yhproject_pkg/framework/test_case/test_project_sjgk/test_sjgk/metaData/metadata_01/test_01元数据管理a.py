import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.ysjgl_page import YsjglPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('元数据管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_ysjgl(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入元数据管理模块-------------')


@allure.feature('元数据管理')
@allure.title('打开菜单')
def test_menu_ysjgl(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_ysj_ysjgl()
    # 切换iframe
    page.sjgk_ysjgl_iframe()
    page.assert_att("元数据库")
    logger.info('打开菜单成功')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据管理分组')
def test_ysjcls_ysjgl(browser, data):
    page = YsjglPage(browser)
    page.icon_ysjcls()
    page.button_ysjcls(data["clsname"], data["clscode"])
    page.assert_att("操作成功")
    logger.info('新增元数据管理分组')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据设置规则')
def test_ysjruleset_ysjgl(browser, data):
    page = YsjglPage(browser)
    page.input_searchcls(data["clsname"])
    page.local_datacls()
    page.button_addruleset()
    page.input_ruleset(data["ruleName"], data["dataName"])
    page.button_alltable()
    page.button_right()
    page.button_comfirm()
    page.assert_att("操作成功")
    logger.info('新增元数据设置规则')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据字段查看')
def test_ysjreview(browser, data):
    page = YsjglPage(browser)
    page.input_searchcls(data["clsname"])
    page.assert_att("元数据库管理")
    page.view_table(data["tablename"], data["fieldname"])
    page.assert_att(data["tablename"])
    logger.info('查看元数据表字段')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据编辑中文表名')
def test_ysjtablename(browser, data):
    page = YsjglPage(browser)
    page.edit_table()
    page.input_tablechiname(data["tablechiname"])
    page.button_edsave()
    page.assert_att(data["tablechiname"])
    logger.info('编辑表中文名')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据编辑表分类')
def test_ysjtableclsname(browser, data):
    page = YsjglPage(browser)
    page.edit_table()
    page.button_tablecls()
    page.input_tableclsname(data["tableclsname"])
    page.check_tableclsname()
    page.button_tableclssave()
    page.button_edsave()
    page.assert_att(data["tableclsname"])
    logger.info('编辑表分类')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据查看字段名')
def test_ysjfieldview(browser, data):
    page = YsjglPage(browser)
    page.view_field()
    page.assert_att(data["fieldname"])
    logger.info('查看元数据字段')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据编辑字段分类')
def test_ysjfieldcls(browser, data):
    page = YsjglPage(browser)
    page.button_editfield()
    page.edit_fieldcls()
    page.input_datacode(data["datacode"])
    page.checkbox_datacode()
    page.button_datacodesave()
    page.button_fieldsave()
    page.assert_att(data["dataname"])
    logger.info('编辑字段分类')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据关联枚举值')
def test_ysjfieldenum(browser, data):
    page = YsjglPage(browser)
    page.button_editemun()
    page.input_dictcode(data["dictcode"])
    page.checkbox_dictcode()
    page.button_dictcomfirm()
    logger.info('字段关联枚举值')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据修改字段备注')
def test_fieldremark(browser, data):
    page = YsjglPage(browser)
    page.button_editfield()
    page.input_fieldremark(data["fieldremark"])
    page.button_fieldcomfirm()
    page.assert_att(data["fieldremark"])
    logger.info('元数据修改字段备注')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据枚举值查看')
def test_ysjcheckenum(browser, data):
    page = YsjglPage(browser)
    page.scroll_right()
    page.assert_att(data["dictcode"])
    page.link_datadict()
    page.assert_att("字段枚举值信息")
    page.icon_closedictcode()
    logger.info('元数据枚举值查看')


@allure.feature('元数据管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/ysjgl.yaml'))
@allure.title('元数据查看历史记录')
def test_historyback(browser, data):
    page = YsjglPage(browser)
    page.button_historyremark()
    page.input_dataname(data["dataName"])
    page.input_tablename(data["tablename"])
    page.input_fieldname(data["fieldname"])
    page.button_historysearch()
    page.assert_att(data["fieldname"])
    page.button_historyback()
    page.assert_att("元数据库管理")
    logger.info('元数据查看历史记录')


@allure.feature('元数据管理')
@allure.title('退出登录')
def test_sjygl_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
