import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.bzfg_page import BzfgPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('标准覆盖')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_bzfg(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入数据标准管理模块-------------')


@allure.feature('标准覆盖')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('打开菜单')
def test_menu_bzfg(browser, data):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjbz_bzfg()
    # 切换iframe
    page.sjgk_bzfg_iframe()
    page.assert_att("落标查询")
    logger.info('01进入菜单成功')


@allure.feature('标准覆盖')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/bzfg.yaml'))
@allure.title('选择落标范围')
def test_searchrange(browser, data):
    page = BzfgPage(browser)
    page.icon_range()
    page.down_keys()
    page.down_keys()
    page.enter_keys()
    page.button_localsearch()
    page.assert_att("字符型")


@allure.feature('标准覆盖')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/bzfg.yaml'))
@allure.title('标准查询')
def test_searchstandard(browser, data):
    page = BzfgPage(browser)
    page.input_standardname(data["standardname"])
    page.assert_att(data["standardname"])


@allure.feature('标准覆盖')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/bzfg.yaml'))
@allure.title('标准关联字段')
def test_relatedfield(browser, data):
    page = BzfgPage(browser)
    page.icon_relatedfield()
    page.assert_att("关联字段")
    page.input_relatedfield(data["relatedfield"])
    page.assert_att(data["standardname"])
    page.icon_fieldclosed()


@allure.feature('标准覆盖')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/bzfg.yaml'))
@allure.title('标准推荐关联')
def test_relatedrecommend(browser, data):
    page = BzfgPage(browser)
    page1 = MenuPage(browser)
    page.icon_recommendrelated()
    page.assert_att("推荐关联")
    page.icon_ownertable()
    page.reback_iframe()
    page1.sjgk_ysjgl_iframe()
    page.assert_att("元数据库管理")
    page.reback_iframe()
    page.icon_ysjtabclosed()
    page1.sjgk_bzfg_iframe()
    page.icon_recommendclosed()
    page.assert_att("字符型")


@allure.feature('标准覆盖')
@allure.title('退出登录')
def test_standard_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')




