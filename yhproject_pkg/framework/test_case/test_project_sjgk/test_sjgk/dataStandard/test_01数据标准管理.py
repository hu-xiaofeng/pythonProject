import pytest
import random
import string
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.sjbzgl_page import SjbzglPage
from time import strftime
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('数据标准管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_sjbzgl(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入数据标准管理模块-------------')


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('打开菜单')
def test_menu_sjbzgl(browser, data):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjbz_sjbzgl()
    # 切换iframe
    page.sjgk_sjbzgl_iframe()
    page.assert_att("数据标准管理")
    logger.info('01进入菜单成功')


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('新增标准1')
def test_addstandard01(browser, data):
    page = SjbzglPage(browser)
    page.button_addstanard()
    page.input_standardchinese(data["standardChinese"])
    page.input_standardenglish(data["standardEnglish"])
    page.input_standardkeys(data["standardKeys"])
    page.input_dataclass()
    page.up_keys()
    page.enter_keys()
    page.input_dataformat()
    page.down_keys()
    page.enter_keys()
    page.input_lengthconstrain(data["length"])
    page.input_accuracyconstrain(data["accuracy"])
    page.button_standardsave()
    page.assert_att("保存成功")


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('新增标准2')
def test_addstandard02(browser, data):
    page = SjbzglPage(browser)
    page.button_addstanard()
    page.input_standardchinese(data["standardchinese1"])
    page.button_viewwords()
    page.assert_att("词根库")
    page.input_words(data["standardchinese1"])
    page.assert_att(data["standardchinese1"])
    page.icon_closed()
    page.button_matchwords()
    page.button_checkwords()
    page.input_standardkeys(data["standardKeys"])
    page.input_dataclass()
    page.down_keys()
    page.down_keys()
    page.enter_keys()
    page.input_dataformat()
    page.down_keys()
    page.enter_keys()
    page.input_lengthconstrain(data["length"])
    page.input_accuracyconstrain(data["accuracy"])
    page.input_relateddict()
    page.input_dictcode(data["dictcode"])
    page.button_dictsearch()
    page.check_diccode()
    page.button_dictcomfirm()
    page.button_standardsave()
    page.assert_att("保存成功")


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('查询标准')
def test_seachstandard01(browser, data):
    page = SjbzglPage(browser)
    page.input_searchstandard(data["standardChinese"])
    page.button_searchstandard()
    page.assert_att(data["standardChinese"])
    page.button_reset()
    page.input_searchstandard(data["standardchinese1"])
    page.button_searchstandard()
    page.assert_att(data["standardchinese1"])
    page.button_reset()


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('配置数据项')
def test_configdata(browser, data):
    page = SjbzglPage(browser)
    page.button_configure()
    page.input_clsdata(data["clsdata"])
    page.icon_checkdata()
    page.button_clssave()
    page.assert_att("操作成功")


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('查看数据标准')
def test_viewstandard(browser, data):
    page = SjbzglPage(browser)
    page.button_viewdata()
    page.down_mouse0()
    page.assert_att(data["clsdata"])
    page.button_closeddata()


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('启停标准')
def test_switch(browser, data):
    page = SjbzglPage(browser)
    page.button_switch()
    page.assert_att("启用")
    page.button_switch1()
    page.assert_att("禁用")


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('清除数据项')
def test_clearstandard(browser, data):
    page = SjbzglPage(browser)
    page.button_configure()
    page.clear_configure()
    page.button_clssave()
    page.enter_keys()
    page.assert_att("操作成功")


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('编辑标准1')
def test_editstandard01(browser, data):
    page = SjbzglPage(browser)
    rand = str(random.randint(0, 10000))
    roud1 = random.choice(string.ascii_letters)
    roud2 = random.choice(string.ascii_letters)
    roud3 = random.choice(string.ascii_letters)
    roud4 = random.choice(string.ascii_letters)
    page.button_reset()
    page.input_searchstandard(data["standardChinese"])
    page.button_searchstandard()
    page.button_editstandard()
    page.assert_att("编辑数据标准")
    page.input_editchinese("py"+str(strftime('%Y%m%d'))+rand)
    page.input_editenglish("py"+roud1+roud2+roud3+roud4)
    page.button_step()
    page.input_auditor()
    page.down_keys()
    page.enter_keys()
    page.input_updatereason(data["updatereason"])
    page.input_updateremark(data["updateremark"])
    page.button_updatecomfirm()
    page.assert_att("变更记录管理")


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('编辑标准2')
def test_editstandard02(browser, data):
    page = SjbzglPage(browser)
    rand = str(random.randint(0, 10000))
    roud1 = random.choice(string.ascii_letters)
    roud2 = random.choice(string.ascii_letters)
    roud3 = random.choice(string.ascii_letters)
    roud4 = random.choice(string.ascii_letters)
    page.button_reset()
    page.input_searchstandard(data["standardchinese1"])
    page.button_searchstandard()
    page.button_editstandard()
    page.assert_att("编辑数据标准")
    page.input_editchinese("py"+str(strftime('%Y%m%d'))+rand)
    page.input_editenglish("py"+roud1+roud2+roud3+roud4)
    page.button_step()
    page.input_auditor()
    page.down_keys()
    page.enter_keys()
    page.input_updatereason(data["updatereason"])
    page.input_updateremark(data["updateremark"])
    page.button_updatecomfirm()
    page.assert_att("变更记录管理")


@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('查询标准')
def test_seachstandard02(browser, data):
    page = SjbzglPage(browser)
    page.input_searchstandard(data["standardChinese"])
    page.button_searchstandard()
    page.assert_att("暂无数据")
    page.button_reset()
    page.input_searchstandard(data["standardchinese1"])
    page.button_searchstandard()
    page.assert_att("暂无数据")
    page.button_reset()


# 变更记录管理
@allure.feature('数据标准管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/sjbzgl.yaml'))
@allure.title('变更记录管理')
def test_updaterecord(browser, data):
    page = SjbzglPage(browser)
    page.tab_updatarecord()
    page.input_searchstandrad(str(strftime('%Y%m%d')))
    page.button_search()
    page.assert_att(str(strftime('%Y%m%d')))
    page.button_updatelist()
    page.assert_att("标准变更详情")
    page.icon_updateclosed()


@allure.feature('数据标准管理')
@allure.title('退出登录')
def test_standard_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
