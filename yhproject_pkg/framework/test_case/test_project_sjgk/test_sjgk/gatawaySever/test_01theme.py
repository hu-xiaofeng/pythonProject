import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.theme_page import ThemePage
from time import strftime
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('主题管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_theme(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入主题管理模块-------------')


@allure.feature('主题管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('打开菜单')
def test_menu_theme(browser, data):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjfw_sjfw_ywztgl()
    # 切换iframe
    page.sjfw_yeztgl_iframe()
    page.assert_att("主题名称")
    logger.info('01进入菜单成功')


@allure.feature('主题管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/theme.yaml'))
@allure.title('新增主题')
def test_add_theme(browser, data):
    page = ThemePage(browser)
    page.button_addtheme()
    page.input_themecode(data['themecode'])
    page.input_themename(data['themename'])
    page.input_themesort(data['themesort'])
    page.input_themedescri(data['themedesc'])
    page.button_themecomfirm()
    page.assert_att("操作成功")


@allure.feature('主题管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/theme.yaml'))
@allure.title('查询主题')
def test_search_theme(browser, data):
    page = ThemePage(browser)
    page.input_searchname(data['themename'])
    page.assert_att(data['themecode'])
    page.wait(1)


@allure.feature('主题管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/theme.yaml'))
@allure.title('编辑主题')
def test_edit_theme(browser, data):
    page = ThemePage(browser)
    page.button_edit()
    page.input_editname(data['editthemename'])
    page.button_themecomfirm()
    page.assert_att("操作成功")


@allure.feature('主题管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/theme.yaml'))
@allure.title('删除主题')
def test_remove_theme(browser, data):
    page = ThemePage(browser)
    page.button_removetheme()
    page.enter_keys()
    page.assert_att("操作成功")


@allure.feature('数据标准管理')
@allure.title('退出登录')
def test_theme_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
