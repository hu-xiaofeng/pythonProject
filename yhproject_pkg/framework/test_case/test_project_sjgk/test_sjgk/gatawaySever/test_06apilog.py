import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.apilog_page import ApiLogPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('接口日志')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_api(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入接口日志模块-------------')


@allure.feature('接口日志')
@allure.title('打开菜单')
def test_menu_api(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjfw_sjfw_jkrz()
    # 切换iframe
    page.sjfw_jkrz_iframe()
    page.assert_att("请求接口名称")
    logger.info('01进入菜单成功')


@allure.feature('接口日志')
@allure.title('搜索查询')
def test_apiquery(browser):
    page = ApiLogPage(browser)
    page.input_username("胡小锋")
    page.button_review()
    page.assert_att("接口调用日志")
    page.button_close()


@allure.feature('接口查询')
@allure.title('退出登录')
def test_apilog_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')






