import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.apionline_page import OnlinePage
from yhproject_pkg.framework.page.sjgk_page.apiadd_page import MicroApiPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('接口上线')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_api(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入接口上线模块-------------')


@allure.feature('接口上线')
@allure.title('打开菜单')
def test_menu_api(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjfw_sjfw_jksxgl()
    # 切换iframe
    page.sjfw_jksxgl_iframe()
    page.assert_att("状态")
    logger.info('01进入菜单成功')


@allure.feature('接口上线')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('查询接口')
def test_search_api(browser, data):
    page = OnlinePage(browser)
    page.input_apiname(data['micapiname'])
    page.button_view()
    page.assert_att("API测试")


@allure.feature('接口上线')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('接口测试')
def test_testapi(browser, data):
    page1 = MicroApiPage(browser)
    page1.tab_params()
    page1.input_key(data['keys'])
    page1.input_values(data['values'])
    page1.button_testapitest2()
    page1.button_reback2()


@allure.feature('接口上线')
@allure.title('接口上下线')
def test_online(browser):
    page = OnlinePage(browser)
    page.button_online()
    page.enter_keys()
    page.assert_att("操作成功")
    page.wait(1)
    page.assert_att("下线")
    page.button_online()
    page.enter_keys()
    page.wait(1)
    page.assert_att("上线")


@allure.feature('接口上线')
@allure.title('退出登录')
def test_apionline_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
