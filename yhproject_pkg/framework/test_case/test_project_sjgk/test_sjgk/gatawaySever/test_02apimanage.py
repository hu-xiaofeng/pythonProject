import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.apiadd_page import ApiPage
from yhproject_pkg.framework.page.sjgk_page.apiadd_page import LocalApiPage
from yhproject_pkg.framework.page.sjgk_page.apiadd_page import MicroApiPage
from yhproject_pkg.framework.page.sjgk_page.apiadd_page import OuterApiPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('接口管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_api(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入接口管理模块-------------')


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('打开菜单')
def test_menu_api(browser, data):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjfw_sjfw_jkcj()
    # 切换iframe
    page.sjfw_jkcj_iframe()
    page.assert_att("创建接口")
    logger.info('01进入菜单成功')


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('本地接口引流')
def test_local_api(browser, data):
    page = ApiPage(browser)
    page.button_addapi()
    page.button_localapi()
    page.assert_att("数据来源")


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('创建本地接口')
def test_add_localapi(browser, data):
    page = LocalApiPage(browser)
    page.input_datasource(data["datasource"])
    page.tab_sqlys()
    page.sql_sqledit()
    page.shift_keys()
    page.shift_keys()
    page.shift_keys()
    page.input_sql()
    page.button_apitest()
    page.icon_close()
    page.input_apiname(data["apiname"])
    page.input_themename(data["themename"])
    page.input_uri(data["uri"])
    page.button_save()
    page.assert_att("操作成功")
    page.wait(1)


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('微服务接口引流')
def test_micro_api(browser, data):
    page = ApiPage(browser)
    page.button_addapi()
    page.button_miniapi()
    page.assert_att("创建微服务接口")


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('创建微服务接口')
def test_add_microapi(browser, data):
    page = MicroApiPage(browser)
    page.input_apiname(data['micapiname'])
    page.input_uri(data['micuri'])
    page.input_theme(data['themename'])
    page.input_url(data['micurl'])
    page.input_request()
    page.down_keys()
    page.enter_keys()
    page.button_apitest()
    page.tab_params()
    page.input_key(data['keys'])
    page.input_values(data['values'])
    page.button_run()
    page.button_close()
    page.button_save()
    page.assert_att("操作成功")
    page.wait(1)


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('外部服务接口引流')
def test_outer_api(browser, data):
    page = ApiPage(browser)
    page.button_addapi()
    page.button_outerapi()
    page.assert_att("创建外部接口")


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('创建外部接口')
def test_add_outerapi(browser, data):
    page = OuterApiPage(browser)
    page.input_apiname(data['outerapiname'])
    page.input_uri(data['outeruri'])
    page.input_theme(data['themename'])
    page.input_url(data['outerurl'])
    page.input_request()
    page.up_keys()
    page.enter_keys()
    page.button_apitest()
    page.tab_headers()
    page.input_hkey(data['hkeys'])
    page.input_hvalues(data['hvalues'])
    page.tab_params()
    page.input_key(data['outerkeys'])
    page.input_values(data['outervalues'])
    page.button_run()
    page.button_close()
    page.button_save()
    page.assert_att("操作成功")
    page.wait(1)


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('查询接口')
def test_search_api(browser, data):
    page = ApiPage(browser)
    page.input_searchapi(data['apiname'])
    page.assert_att(data["apiname"])


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('修改接口')
def test_updateapi(browser, data):
    page = ApiPage(browser)
    page1 = LocalApiPage(browser)
    page.icon_todo()
    page.button_update()
    page.input_editapiname("s")
    page1.button_apitest()
    page1.icon_close()
    page1.button_save()
    page.assert_att("操作成功")


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('测试接口')
def test_testapi(browser, data):
    page = ApiPage(browser)
    page1 = MicroApiPage(browser)
    page.button_resetapi()
    page.input_searchapi(data['micapiname'])
    page.icon_todo()
    page.button_test()
    page1.tab_params()
    page1.input_key(data['keys'])
    page1.input_values(data['values'])
    page1.button_testapitest()
    page1.button_reback()


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('提交接口')
def test_commitapi(browser, data):
    page = ApiPage(browser)
    page.button_resetapi()
    page.input_searchapi(data['micapiname'])
    page.button_commit()
    page.enter_keys()
    page.assert_att("操作成功")


@allure.feature('接口管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('查看接口')
def test_viewapi(browser, data):
    page = ApiPage(browser)
    page.button_resetapi()
    page.input_searchapi(data['micapiname'])
    page.button_commit()
    page.assert_att("接口信息")
    page.button_reback()


@allure.feature('接口管理')
@allure.title('退出登录')
def test_apimanage_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
