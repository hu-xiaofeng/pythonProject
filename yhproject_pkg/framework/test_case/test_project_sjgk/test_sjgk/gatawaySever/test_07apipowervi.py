import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.apipowervi_page import PowerViPage
from yhproject_pkg.framework.page.sjgk_page.apionline_page import OnlinePage
from yhproject_pkg.framework.page.sjgk_page.apiadd_page import ApiPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('权限查看')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_api(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入接口日志模块-------------')


@allure.feature('权限查看')
@allure.title('打开菜单')
def test_menu_api(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjfw_sjfw_qxck()
    # 切换iframe
    page.sjfw_qxck_iframe()
    page.assert_att("接口名称")
    logger.info('01进入菜单成功')


@allure.feature('权限查看')
@allure.title('检查授权')
def test_reviewpower(browser):
    page = PowerViPage(browser)
    page.input_apiname("外部接口get")
    page.input_username("胡小锋")
    page.button_search()
    page.assert_att("上线")


@allure.feature('权限查看')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('接口下线')
def test_outline_api(browser, data):
    page = MenuPage(browser)
    page1 = OnlinePage(browser)
    page.reback_iframe()
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjfw_sjfw_jksxgl()
    # 切换iframe
    page.sjfw_jksxgl_iframe()
    page1.input_apiname(data['micapiname'])
    page1.button_online()
    page1.enter_keys()
    page1.assert_att("操作成功")
    page1.wait(1)
    page1.assert_att("离线")


@allure.feature('权限查看')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('接口删除')
def test_remove_api(browser, data):
    page = MenuPage(browser)
    page1 = ApiPage(browser)
    page.reback_iframe()
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjfw_sjfw_jkcj()
    # 切换iframe
    page.sjfw_jkcj_iframe()
    page1.input_searchapi(data['micapiname'])
    page1.icon_todo()
    page1.button_remove()
    page1.enter_keys()
    page1.button_resetapi()
    page1.input_searchapi(data['apiname'])
    page1.icon_todo()
    page1.button_remove()
    page.enter_keys()
    page1.button_resetapi()
    page1.input_searchapi(data['outerapiname'])
    page1.icon_todo()
    page1.button_remove()
    page.enter_keys()


@allure.feature('权限查看')
@allure.title('退出登录')
def test_apilog_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
