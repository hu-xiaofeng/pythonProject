import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.auth_page.role_page import ApiPowerPage
from yhproject_pkg.framework.page.sjgk_page.apilist_page import ApiListPage
from yhproject_pkg.framework.page.sjgk_page.apiadd_page import MicroApiPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('接口查询')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_api(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入接口查询模块-------------')


@allure.feature('接口查询')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('打开菜单')
def test_menu_api(browser, data):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjfw_sjfw_jkcx()
    # 切换iframe
    page.sjfw_jkcx_iframe()
    page.assert_att("状态")
    logger.info('01进入菜单成功')


@allure.feature('接口查询')
@allure.title('查看秘钥')
def test_viewsecret(browser):
    page = ApiListPage(browser)
    page.button_viewsecret()
    page.assert_att("密钥信息")
    page.button_secretclose()


@allure.feature('接口查询')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('查看接口')
def test_viewapilist(browser, data):
    page = ApiListPage(browser)
    page.input_apiname(data['micapiname'])
    page.assert_att(data['micapiname'])


@allure.feature('接口查询')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('进入角色管理')
def test_rolemenu(browser, data):
    page = MenuPage(browser)
    page.reback_iframe()
    page.table_basic()
    page.menu_icon()
    page.menu_sys()
    page.sys_role()
    # 切换iframe
    page.role_iframe()
    page.assert_att("角色编码")
    logger.info('02进入角色成功')


@allure.feature('接口查询')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/auth/role.yaml'))
@pytest.mark.parametrize('data1', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('角色授权')
def test_apipower(browser, data, data1):
    page = ApiPowerPage(browser)
    page.input_rolename(data["role"])
    page.button_apipower()
    page.input_apiname(data1["micapiname"])
    page.checkbox_apiname()
    page.button_comfirm()
    page.assert_att("操作成功")


@allure.feature('接口查询')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('检查授权')
def test_reviewpower(browser, data):
    page = ApiListPage(browser)
    page1 = MenuPage(browser)
    page1.reback_iframe()
    page.tab_apilist()
    page1.sjfw_jkcx_iframe()
    page1.wait(3)
    page.button_reset()
    page.input_apiname(data["micapiname"])
    page.assert_att("可用")


@allure.feature('接口查询')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/auth/role.yaml'))
@allure.title('接口查看')
def test_apiview(browser, data):
    page = ApiListPage(browser)
    page.button_view()
    page.assert_att("API测试")


@allure.feature('接口查询')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('接口测试')
def test_testapi(browser, data):
    page1 = MicroApiPage(browser)
    page1.tab_params()
    page1.input_key(data['keys'])
    page1.input_values(data['values'])
    page1.button_testapitest3()
    page1.button_reback3()
    page1.reback_iframe()


@allure.feature('接口查询')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/auth/role.yaml'))
@pytest.mark.parametrize('data1', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/jkcj.yaml'))
@allure.title('恢复权限')
def test_reviewpower1(browser, data, data1):
    page = MenuPage(browser)
    page1 = ApiPowerPage(browser)
    page2 = ApiListPage(browser)
    page2.tab_role()
    page.role_iframe()
    page1.button_reset()
    page1.input_rolename(data["role"])
    page1.button_apipower()
    page1.input_apiname(data1["micapiname"])
    page1.checkbox_apiname()
    page1.button_comfirm()
    page1.assert_att("操作成功")


@allure.feature('接口查询')
@allure.title('退出登录')
def test_apiview_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')

