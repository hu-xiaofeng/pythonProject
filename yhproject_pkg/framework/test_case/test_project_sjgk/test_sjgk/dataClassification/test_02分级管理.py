import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.fjgl_page import FjglPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('分级管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_sjfjgl(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.wait(3)
    page.assert_att("数据管控平台")
    logger.info('------------进入数据标准管理模块-------------')


@allure.feature('分级管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('打开菜单')
def test_menu_sjfjgl(browser, data):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjflfj_sjfj()
    # 切换iframe
    page.sjgk_sjfj_iframe()
    page.assert_att("分级管理")
    logger.info('01进入菜单成功')


@allure.feature('分级管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/fjgl.yaml'))
@allure.title('新增分级树')
def test_fjtree(browser, data):
    page = FjglPage(browser)
    page.button_treeadd()
    page.input_treerefer(data["treerefer"])
    page.input_treename(data["treename"])
    page.input_treeremark(data["treeremark"])
    page.button_treesave()
    page.assert_att("操作成功")


@allure.feature('分级管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/fjgl1.yaml'))
@allure.title('新增分级')
def test_addlevel(browser, data):
    page = FjglPage(browser)
    page.input_treesearch(data["treename"])
    page.local_tree()
    page.button_addlevel()
    page.input_levelname(data["levelName"])
    page.input_datafeature(data["dataFeature"])
    page.input_instance(data["instance"])
    page.input_object(data["object"])
    page.input_scope(data["scope"])
    page.input_extent(data["extent"])
    page.input_import(data["import"])
    page.input_datalevel(data["level"])
    page.input_levelremark(data["remark"])
    page.button_levelsave()
    page.assert_att("操作成功")


@allure.feature('分级管理')
@allure.title('查询分级')
def test_searchlevel(browser):
    page = FjglPage(browser)
    page.input_searchlevel("国家安全级别5")
    page.button_updatelevel()
    page.input_levelname("国家安全级别6")
    page.button_levelsave()
    page.assert_att("操作成功")


@allure.feature('分级管理')
@allure.title('删除分级')
def test_removelevel(browser):
    page = FjglPage(browser)
    page.button_reset()
    page.input_searchlevel("国家安全级别6")
    page.button_removelevel()
    page.assert_att("操作成功")


@allure.feature('分级管理')
@allure.title('分级映射')
def test_maplevel(browser):
    page = FjglPage(browser)
    page.button_map()
    page.assert_att("赢和")


@allure.feature('分级管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/fjgl.yaml'))
@allure.title('编辑分级树')
def test_edittree(browser, data):
    page = FjglPage(browser)
    page.input_treesearch(data["treename"])
    page.local_tree()
    page.local_oper()
    page.button_edittree()
    page.input_treename(data["treename1"])
    page.button_treesave()
    page.assert_att("操作成功")


@allure.feature('分级管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/fjgl.yaml'))
@allure.title('删除分级树')
def test_removetree(browser, data):
    page = FjglPage(browser)
    page.input_treesearch(data["treename1"])
    page.local_tree()
    page.local_oper()
    page.button_removetree()
    page.enter_keys()
    page.assert_att("操作成功")


@allure.feature('分级管理')
@allure.title('退出登录')
def test_fjgl_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
