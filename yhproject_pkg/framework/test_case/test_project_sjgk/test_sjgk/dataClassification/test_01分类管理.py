import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjgk_page.flgl_page import FlglPage
from time import strftime
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('分类管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_sjflgl(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.wait(3)
    page.assert_att("数据管控平台")
    logger.info('------------进入分类管理模块-------------')


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/menu.yaml'))
@allure.title('打开菜单')
def test_menu_sjflgl(browser, data):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjgk()
    page.sjgk_sjflfj_sjfl()
    # 切换iframe
    page.sjgk_sjfl_iframe()
    page.assert_att("数据分类")
    logger.info('01进入菜单成功')


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('新增分类树')
def test_addtree(browser, data):
    page = FlglPage(browser)
    page.icon_opertree()
    page.icon_addtree()
    page.input_clsname(data["clsname"])
    page.input_clsremark(data["clsremark"])
    page.icon_addtitle()
    page.icon_addtitle()
    page.icon_addtitle()
    page.icon_addtitle()
    page.icon_addtitle()
    page.input_title1(data["title1"])
    page.input_titleremark1(data["titleremark1"])
    page.input_title2(data["title2"])
    page.input_titleremark2(data["titleremark2"])
    page.input_title3(data["title3"])
    page.input_titleremark3(data["titleremark3"])
    page.input_title4(data["title4"])
    page.input_titleremark4(data["titleremark4"])
    page.input_title5(data["title5"])
    page.input_titleremark5(data["titleremark5"])
    page.button_treesave()
    page.closed_tips()
    page.wait(5)


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('编辑分类树')
def test_edittree(browser, data):
    page = FlglPage(browser)
    page.button_edittree()
    page.input_clsremark("editremark")
    page.button_treesave()
    page.closed_tips()
    page.wait(5)


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('新增叶子')
def test_addleaf(browser, data):
    page = FlglPage(browser)
    page.icon_opertree()
    page.icon_addleaf()
    page.input_leafname(data["leafname"])
    page.input_leafremark(data["leafremark"])
    page.button_leafsave()
    page.wait(10)


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('编辑叶子')
def test_editleaf(browser, data):
    page = FlglPage(browser)
    page.button_localleaf()
    page.wait(2)
    page.button_editleaf()
    page.input_editleafremark("editremark")
    page.button_leafsave()
    page.assert_att("操作成功")
    page.wait(3)
    page.closed_tips()


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('新增树节点')
def test_addleaf1(browser, data):
    page = FlglPage(browser)
    page.button_localleaf()
    page.icon_operleaf1()
    page.button_addleaf1()
    page.input_leafname1(data["leafname1"])
    page.button_leafsave1()
    page.assert_att("操作成功")
    page.wait(3)
    page.closed_tips()


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('新增数据项')
def test_adddata(browser, data):
    page = FlglPage(browser)
    page.button_localleaf1()
    page.icon_operleaf1()
    page.button_adddata()
    page.input_dataitem1(data["dataitem1"])
    page.input_dataremark1(data["dataremark1"])
    page.button_datasave()
    page.assert_att("操作成功")


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('挂载')
def test_mount(browser, data):
    page = FlglPage(browser)
    page.button_localleaf1()
    page.icon_operleaf1()
    page.button_mount()
    page.button_firstmount()
    page.assert_att("操作成功")


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('调整层级')
def test_change(browser, data):
    page = FlglPage(browser)
    page.button_localleaf1()
    page.icon_operleaf1()
    page.button_chang()
    page.icon_firstitem()
    page.button_changsave()
    page.assert_att("操作成功")


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('删除节点')
def test_remove(browser, data):
    page = FlglPage(browser)
    page.button_localleaf1()
    page.icon_operleaf1()
    page.button_remove()
    page.enter_keys()
    page.assert_att("操作成功")


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('删除树')
def test_treeremove(browser, data):
    page = FlglPage(browser)
    page.icon_opertree()
    page.icon_removetree()
    page.enter_keys()
    page.assert_att("操作成功")
    page.wait(10)


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('新增主树数据项')
def test_adddatacode(browser, data):
    page = FlglPage(browser)
    page.input_datacode1(data["leafname2"])
    page.local_cls()
    page.wait(3)
    page.icon_oprecls()
    page.button_adddata1()
    page.assert_att("数据项信息")
    page.input_dataitem(data["maindataitem1"])
    page.input_dataremark(data["dataremark"])
    page.input_level(data["level"])
    page.input_standard(data["standard"])
    page.input_dataproduct(data["product"])
    page.input_datacustomer(data["customer"])
    page.input_datatag(data["tag"])
    page.icon_adddatalist()
    page.input_datakeys(data["keys"])
    page.input_datalocal(data["local"])
    page.input_datauser(data["user"])
    page.icon_metadate()
    page.input_metadata(data["metadata"])
    page.input_tablename(data["tablename"], data["fieldname"])
    page.check_fields()
    page.button_fieldsave()
    page.button_datasave1()
    page.assert_att("操作成功")
    page.wait(5)


@allure.feature('分类管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjgk/flgl.yaml'))
@allure.title('删除主树数据项')
def test_removedatacode(browser, data):
    page = FlglPage(browser)
    page.input_datacode1(data["maindataitem1"])
    page.local_datacode()
    page.remove_datacode()
    page.enter_keys()
    page.assert_att("操作成功")


@allure.feature('分类管理')
@allure.title('退出登录')
def test_flgl_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
