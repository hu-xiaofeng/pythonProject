import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjkf_page.sjygl_page import SjyglPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('数据源管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_sjygl(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    page.wait(2)
# 登录跳转
    page.switch_window(-1)
    page.assert_att("赢和·数据管控平台")
    logger.info('------------进入数据源管理模块-------------')


@allure.feature('数据源管理')
@allure.title('打开菜单_数据源管理')
def test_menu_sjygl(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjkf()
    page.sjkf_cjzx_sjygl()
    logger.info('打开菜单_数据源管理')
# 切换iframe
    page.sjgk_sjygl_iframe()
    page.assert_att("数据源列表")
    logger.info('01打开菜单_数据源管理')


@allure.feature('数据源管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/sjygl.yaml'))
@allure.title('数据源管理_删除数据源1')
def test_stopdata(browser, data):
    page = SjyglPage(browser)
    page.input_search(data['fzmc'])
    page.local_datasource()
    page.data_todo()
    page.data_switch2()
    page.assert_att("操作成功")
    page.data_todo()
    page.data_remove2()
    page.enter_keys()
    page.assert_att("操作成功")
    logger.info('删除数据源1')


@allure.feature('数据源管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/sjygl.yaml'))
@allure.title('数据源管理_删除数据源2')
def test_stopdata1(browser, data):
    page = SjyglPage(browser)
    page.input_search(data['fzmc'])
    page.local_datasource()
    page.data_todo()
    page.data_switch2()
    page.assert_att("操作成功")
    page.data_todo()
    page.data_remove2()
    page.enter_keys()
    page.assert_att("操作成功")
    logger.info('删除数据源2')


@allure.feature('数据源管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/sjygl.yaml'))
@allure.title('数据源管理_删除分组')
def test_removecls(browser, data):
    page = SjyglPage(browser)
    page.input_search(data['fzmc'])
    page.local_datasource()
    page.opera_datasource()
    page.remove_cls()
    page.enter_keys()
    page.assert_att("操作成功")
    logger.info('删除分组')


@allure.feature('数据源管理')
@allure.title('退出登录')
def test_sjygl_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
