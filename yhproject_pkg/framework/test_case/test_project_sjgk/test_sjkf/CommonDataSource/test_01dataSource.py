import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjkf_page.sjygl_page import SjyglPage
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('数据源管理')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_sjygl(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    page.wait(2)
# 登录跳转
    page.switch_window(-1)
    page.assert_att("赢和·数据管控平台")
    logger.info('------------进入数据源管理模块-------------')


@allure.feature('数据源管理')
@allure.title('打开菜单_数据源管理')
def test_menu_sjygl(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjkf()
    page.sjkf_cjzx_sjygl()
    logger.info('打开菜单_数据源管理')
# 切换iframe
    page.sjgk_sjygl_iframe()
    page.assert_att("数据源列表")
    logger.info('01打开菜单')


@allure.feature('数据源管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/sjygl.yaml'))
@allure.title('数据源管理_新增业务分组')
def test_addywfc(browser, data):
    page = SjyglPage(browser)
    page.icon_addcls()
    page.input_clsname(data['fzmc'])
    page.assert_att("操作成功")
    logger.info('02新增业务分层')


@allure.feature('数据源管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/sjygl.yaml'))
@allure.title('数据源管理_查询业务分组')
def test_searchywfc(browser, data):
    page = SjyglPage(browser)
    page.input_search(data['fzmc'])
    page.assert_att(data['fzmc'])
    logger.info('03查询业务分层')


@allure.feature('数据源管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/sjygl.yaml'))
@allure.title('数据源管理_查询业务分组')
def test_adddata(browser, data):
    page = SjyglPage(browser)
    page.local_datasource()
    page.opera_datasource()
    page.add_datasource()
    page.assert_att("新增数据源")
    logger.info('04新增数据源')


@allure.feature('数据源管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/sjygl.yaml'))
@allure.title('数据源管理_新增oracle')
def test_addora(browser, data):
    page = SjyglPage(browser)
    page.add_oracle(data["dataname"], data["dataid"], data["host"], data["port"], data["username"], data["password"])
    page.test_join()
    page.assert_att("测试连接成功")
    page.button_orasave()
    page.assert_att("操作成功")
    logger.info('05新增数据源oracle')


@allure.feature('数据源管理')
@allure.title('数据源管理_再次新增数据源')
def test_adddata01(browser):
    page = SjyglPage(browser)
    page.local_datasource()
    page.opera_datasource()
    page.add_datasource()
    page.assert_att("新增数据源")
    logger.info('06新增数据源')


@allure.feature('数据源管理')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/sjygl.yaml'))
@allure.title('数据源管理_新增hive')
def test_addhive(browser, data):
    page = SjyglPage(browser)
    page.add_hive(data["dataname_hive"], data["dataid_hive"], data["host_hive"], data["port_hive"], data["url_hive"])
    page.test_join()
    page.assert_att("连接成功")
    page.button_hivesave()
    page.assert_att("操作成功")
    logger.info('07新增数据源hive')


@allure.feature('数据源管理')
@allure.title('数据源管理_查看')
def test_viewdata(browser):
    page = SjyglPage(browser)
    page.button_view()
    page.assert_att("详情")
    page.view_testjoin()
    page.assert_att("连接成功")
    page.view_switch()
    page.assert_att("操作成功")
    page.view_edit()
    page.assert_att("编辑")
    page.view_back()
    page.assert_att("查看")
    logger.info('08数据源详情页面操作')
    page.wait(1)


@allure.feature('数据源管理')
@allure.title('数据源管理_操作')
def test_tododata(browser):
    page = SjyglPage(browser)
    page.data_todo()
    page.data_join()
    page.assert_att("连接成功")
    page.data_todo()
    page.data_edit()
    page.assert_att("编辑")
    page.view_back()
    page.data_todo()
    page.data_copy()
    page.assert_att("复制")
    page.view_cpback()
    page.data_todo()
    page.data_switch1()
    page.assert_att("操作成功")
    logger.info('09数据源操作列操作')


@allure.feature('数据源管理')
@allure.title('退出登录')
def test_sjygl_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')


