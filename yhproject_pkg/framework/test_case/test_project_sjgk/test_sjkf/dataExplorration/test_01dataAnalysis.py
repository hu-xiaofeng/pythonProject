import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjkf_page.tyfx_page import DataList
from yhproject_pkg.framework.page.sjkf_page.tyfx_page import DataView
from yhproject_pkg.framework.page.sjkf_page.tyfx_page import DataEdit
import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y%m%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('探源分析')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入探源分析模块-------------')


@allure.feature('探源分析')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjkf()
    page.sjkf_sjty_tyfx()
    # 切换iframe
    page.sjkf_tyfx_iframe()
    page.assert_att("数据源列表")
    logger.info('01进入菜单成功')


@allure.feature('探源分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('进入数据源详情')
def test_query_data(browser, data):
    page = DataList(browser)
    page.input_datasource(data['datasource'])
    page.wait(1)
    page.local_datasource()
    page.assert_att("数据源详情")


@allure.feature('探源分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('查看源表结构')
def test_viewtable(browser, data):
    page = DataView(browser)
    page.input_tablename(data['tablename'])
    page.button_viewtable()
    page.assert_att("查看源表结构")
    page.button_closed()
    page.wait(1)


@allure.feature('探源分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('探源分析入口')
def test_analaysis_data(browser, data):
    page = DataView(browser)
    page.checkbox_datalist()
    page.button_addtask()
    page.assert_att("创建任务")


@allure.feature('探源分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('创建任务')
def test_addtask_data(browser, data):
    page = DataView(browser)
    page.input_themename("theme"+now1)
    page.button_taskcomfirm()
    page.assert_att("探源分析")


@allure.feature('探源分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('保存草稿')
def test_draft(browser, data):
    page = DataEdit(browser)
    page.input_classfy(data['classfy'])
    page.input_classview(data['classview'])
    page.box_check()
    page.button_savedraft()
    page.assert_att("操作成功")
    page.button_reback()


@allure.feature('探源分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('探源编辑入口')
def test_analaysis_data2(browser, data):
    page = DataView(browser)
    page1 = DataList(browser)
    page1.tab_tree1()
    page.input_tablename(data['tablename'])
    page.checkbox_datalist()
    page.button_editdata()
    page.assert_att("探源分析")


@allure.feature('探源分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('表信息编辑')
def test_edittable(browser, data):
    page = DataEdit(browser)
    page.button_tableedit()
    page.input_tablechinese(data['tablename']+now1)
    page.button_comfirm()
    page.assert_att("保存成功")


@allure.feature('探源分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('字段信息编辑')
def test_editfield(browser, data):
    page = DataEdit(browser)
    page.button_fieldedit()
    page.button_addfield()
    page.input_field(data['field']+now1)
    page.input_fieldname(data['fieldname']+now1)
    page.input_fieldtype(data['fieldtype'])
    page.input_length(data['length'])
    page.button_eidtswitchtype()
    page.button_fieldcomfirm()
    page.assert_att("保存成功")


@allure.feature('探源分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('ODS表预览')
def test_odsview(browser, data):
    page = DataEdit(browser)
    page.button_dataview()
    page.assert_att(data['fieldname']+now1)
    page.button_odsclose()


@allure.feature('探源分析')
@allure.title('提交复核')
def test_commit(browser):
    page = DataEdit(browser)
    page.button_commit()
    page.assert_att("操作成功")
    page.button_reback()


@allure.feature('探源分析')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('检查复核状态')
def test_checkstatus(browser, data):
    page = DataView(browser)
    page.input_tablename(data['tablename'])
    page.scroll_left()
    page.assert_att("复核中")


@allure.feature('探源分析')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
