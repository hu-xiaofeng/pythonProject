import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjkf_page.tyjk_page import DataMonitor
from yhproject_pkg.framework.page.sjkf_page.tyfh_page import DataCheck
from yhproject_pkg.framework.page.sjkf_page.tyfx_page import DataView
from yhproject_pkg.framework.page.sjkf_page.tyfx_page import DataList
from yhproject_pkg.framework.page.sjkf_page.tyfx_page import DataEdit

import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y%m%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('探源监控')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入探源监控模块-------------')


@allure.feature('探源监控')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjkf()
    page.sjkf_sjty_tyjk()
    # 切换iframe
    page.sjkf_tyjk_iframe()
    page.assert_att("监控任务列表")
    logger.info('01进入菜单成功')


@allure.feature('探源监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyjk.yaml'))
@pytest.mark.parametrize('data1', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('搜索元数据源')
def test_search_data(browser, data, data1):
    page = DataMonitor(browser)
    page.input_datalist(data["data"])
    page.local_data()
    page.assert_att(data["data"])


@allure.feature('探源监控')
@pytest.mark.parametrize('data1', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('搜索元数据表')
def test_search_table2(browser, data1):
    page = DataMonitor(browser)
    page.input_tablename(data1['tablename'])
    page.icon_expand()
    page.wait(3)
    page.assert_att("自动测试")


@allure.feature('探源监控')
@pytest.mark.parametrize('data1', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('版本管理查看')
def test_viewversion(browser, data1):
    page = DataMonitor(browser)
    page.tab_version()
    page.button_versionview()
    page.assert_att("查看")
    page.icon_checkitems()
    page.input_tables(data1["tablename"])
    page.icon_expand1()
    page.assert_att("字段英文")
    page.icon_closed()


@allure.feature('探源监控')
@pytest.mark.parametrize('data1', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('监控任务')
def test_monitor(browser, data1):
    page = DataMonitor(browser)
    page.tab_tasklist()
    page.button_edit()
    page.assert_att("任务编辑")
    page.button_addtask()
    page.assert_att("前往任务列表")
    page.icon_closed1()


@allure.feature('探源监控')
@pytest.mark.parametrize('data1', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('在线探源')
def test_online(browser, data1):
    page = DataMonitor(browser)
    page.tab_monitor()
    page.button_online()
    page.wait(3)
    page.assert_att("在线探源成功")


@allure.feature('探源监控')
@pytest.mark.parametrize('data1', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('探源编辑')
def test_monitoredit(browser, data1):
    page = DataMonitor(browser)
    page1 = MenuPage(browser)
    page2 = DataList(browser)
    page.tab_monitor()
    page.button_dataedit()
    page.reback_iframe()
    page2.wait(10)
    page2.tab_analysis()
    # 切换iframe
    page1.sjkf_tyfx_iframe1()
    page1.assert_att("任务列表")


@allure.feature('探源监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('再次进入探源编辑')
def test_query_data(browser, data):
    page = DataList(browser)
    page.tab_tree1()
    page.input_datasource(data['datasource'])
    page.wait(1)
    page.local_datasource()
    page.assert_att("数据源详情")


@allure.feature('探源监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('检查复核状态')
def test_checkstatus1(browser, data):
    page = DataView(browser)
    page.button_reset()
    page.input_tablename(data['tablename'])
    page.scroll_left()
    page.assert_att("复核通过")


@allure.feature('探源监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('申请撤销探源')
def test_cancel(browser, data):
    page = DataView(browser)
    page1 = DataEdit(browser)
    page.checkbox_datalist()
    page.button_editdata()
    page1.button_uncommit()
    page1.assert_att("操作成功")
    page1.button_reback()
    page1.assert_att("数据源详情")
    page.button_reset()
    page.input_tablename(data['tablename'])
    page.scroll_left()
    page.assert_att("复核中")
    page.reback_iframe()


@allure.feature('探源监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('撤销探源复核通过')
def test_cancelsuccs(browser, data):
    page = MenuPage(browser)
    page1 = DataCheck(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjkf()
    page.sjkf_sjty_tyfh()
    # 切换iframe
    page.sjkf_tyfh_iframe()
    page.wait(10)
    page1.button_search()
    page1.assert_att("删除")
    page1.button_pass()
    page1.assert_att("操作成功")
    page1.reback_iframe()


@allure.feature('探源监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('再次检查复核状态')
def test_checkstatused(browser, data):
    page = DataView(browser)
    page2 = MenuPage(browser)
    page1 = DataList(browser)
    page1.tab_analysis()
    page2.sjkf_tyfx_iframe1()
    page.button_reset()
    page.input_tablename(data['tablename'])
    page.scroll_left()
    page.assert_att("复核通过")


@allure.feature('探源监控')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('再次提交撤销探源')
def test_cancelsuccsed(browser, data):
    page = DataEdit(browser)
    page1 = DataView(browser)
    page1.checkbox_datalist()
    page1.button_editdata()
    page.button_uncommit()
    page.assert_att("操作成功")
    page.button_reback()
    page1.button_reset()
    page1.input_tablename(data['tablename'])
    page1.scroll_left()
    page1.assert_att("未接入")


@allure.feature('探源监控')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
