import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjkf_page.tyfh_page import DataCheck
from yhproject_pkg.framework.page.sjkf_page.tyfx_page import DataView
from yhproject_pkg.framework.page.sjkf_page.tyfx_page import DataList
from yhproject_pkg.framework.page.sjkf_page.tyfx_page import DataEdit
import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y%m%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('探源复核')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入探源复核模块-------------')


@allure.feature('探源复核')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjkf()
    page.sjkf_sjty_tyfh()
    # 切换iframe
    page.sjkf_tyfh_iframe()
    page.assert_att("待复核")
    logger.info('01进入菜单成功')


@allure.feature('探源复核')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('查看详情')
def test_view_data(browser, data):
    page = DataCheck(browser)
    page.button_view()
    page.assert_att(data['tablename']+now1)
    page.button_cloesed()


@allure.feature('探源复核')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('变更对比')
def test_change_data(browser, data):
    page = DataCheck(browser)
    page.button_changer()
    page.assert_att(data['tablename'])
    page.button_cloesed()


@allure.feature('探源复核')
@allure.title('复核驳回')
def test_check_refuse(browser):
    page = DataCheck(browser)
    page.button_false()
    page.assert_att("操作成功")


@allure.feature('探源复核')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('检查复核状态')
def test_checkstatus_false(browser, data):
    page = MenuPage(browser)
    page1 = DataList(browser)
    page2 = DataView(browser)
    page.reback_iframe()
    page.table_manage()
    page.menu_icon()
    page.menu_sjkf()
    page.sjkf_sjty_tyfx()
    # 切换iframe
    page.sjkf_tyfx_iframe()
    page1.input_datasource(data['datasource'])
    page1.wait(1)
    page1.local_datasource()
    page2.input_tablename(data['tablename'])
    page2.scroll_left()
    page2.assert_att("复核驳回")


@allure.feature('探源复核')
@allure.title('再次提交复核')
def test_commit(browser):
    page = DataView(browser)
    page1 = DataEdit(browser)
    page.checkbox_datalist()
    page.button_editdata()
    page1.button_commit()
    page1.assert_att("操作成功")


@allure.feature('探源复核')
@allure.title('复核通过')
def test_pass(browser):
    page = DataCheck(browser)
    page1 = MenuPage(browser)
    page.reback_iframe()
    page.tab_tyfh()
    page1.sjkf_tyfh_iframe()
    page.button_search()
    page.button_pass()
    page.assert_att("操作成功")


@allure.feature('探源复核')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjkf/tyfx.yaml'))
@allure.title('复核历史')
def test_histoy(browser, data):
    page = DataCheck(browser)
    page.tab_history()
    page.button_hisview()
    page.assert_att(data['tablename']+now1)
    page.icon_closed()


@allure.feature('探源复核')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
