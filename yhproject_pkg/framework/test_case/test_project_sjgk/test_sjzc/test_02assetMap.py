import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjzc_page.zcdt_page import ZcdtPage

import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('资产地图')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入资产地图查看模块-------------')


@allure.feature('资产地图')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjzc()
    page.sjzc_zcdt_zcdt()
    # 切换iframe
    page.sjzc_zcdt_iframe()
    page.assert_att("数据底座")
    logger.info('01进入菜单成功')


@allure.feature('资产地图')
@allure.title('展开数据源底座')
def test_basic_data(browser):
    page = ZcdtPage(browser)
    page.icon_sjdz()
    page.assert_att("数据底座数据源信息")
    page.icon_closed()


@allure.feature('资产地图')
@allure.title('跳转元数据')
def test_ods_data(browser):
    page = ZcdtPage(browser)
    page.icon_ods()
    page.assert_att("元数据")


@allure.feature('资产地图')
@allure.title('跳转数据分类')
def test_classify_data(browser):
    page = ZcdtPage(browser)
    page1 = MenuPage(browser)
    page.reback_iframe()
    page.tab_zcdt()
    page1.sjzc_zcdt_iframe()
    page.icon_jy()
    page.assert_att("数据分类")


@allure.feature('资产地图')
@allure.title('跳转数据标准')
def test_standard_data(browser):
    page = ZcdtPage(browser)
    page1 = MenuPage(browser)
    page.reback_iframe()
    page.tab_zcdt()
    page1.sjzc_zcdt_iframe()
    page.icon_standard()
    page.assert_att("数据标准")


@allure.feature('资产地图')
@allure.title('跳转质量分析')
def test_quality_data(browser):
    page = ZcdtPage(browser)
    page1 = MenuPage(browser)
    page.reback_iframe()
    page.tab_zcdt()
    page1.sjzc_zcdt_iframe()
    page.icon_zlfx()
    page.assert_att("质量分析")


@allure.feature('测试登录')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
