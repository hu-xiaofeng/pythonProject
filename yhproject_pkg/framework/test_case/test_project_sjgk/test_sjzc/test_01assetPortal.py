import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage
from yhproject_pkg.framework.page.sjzc_page.sjmh_page import SjmhPage


import datetime
now = datetime.datetime.now()
now1 = now.strftime("%Y/%m/%d")
import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('数据资产')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    # page.close_window()
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入数据脱敏查看模块-------------')


@allure.feature('数据资产')
@allure.title('打开菜单')
def test_menu_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjzc()
    page.sjzc_sjmh_sjmh()
    # 切换iframe
    page.sjzc_sjmh_iframe()
    page.assert_att("ICEBERG数据服务咨询,让数据更有价值")
    logger.info('01进入菜单成功')


@allure.feature('数据资产')
@allure.title('关键字搜索')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/sjzc/sjmh.yaml'))
def test_keywords_search(browser, data):
    page = SjmhPage(browser)
    page.input_keywords(data['keywords'])
    page.assert_att(data['keywords'])


@allure.feature('数据资产')
@allure.title('元数据跳转')
def test_meta_search(browser):
    page = SjmhPage(browser)
    page.button_meta()
    page.local_data()
    page.assert_att("元数据")


@allure.feature('数据资产')
@allure.title('数据标准跳转')
def test_standard_search(browser):
    page = SjmhPage(browser)
    page1 = MenuPage(browser)
    page.reback_iframe()
    page.tab_zcmh()
    page1.sjzc_sjmh_iframe()
    page.button_standard()
    page.local_data()
    page.assert_att("数据标准")


@allure.feature('数据资产')
@allure.title('数据分类跳转')
def test_classify_search(browser):
    page = SjmhPage(browser)
    page1 = MenuPage(browser)
    page.reback_iframe()
    page.tab_zcmh()
    page1.sjzc_sjmh_iframe()
    page.button_classfiy()
    page.local_data()
    page.assert_att("数据分类")


@allure.feature('数据资产')
@allure.title('数据分级跳转')
def test_level_search(browser):
    page = SjmhPage(browser)
    page1 = MenuPage(browser)
    page.reback_iframe()
    page.tab_zcmh()
    page1.sjzc_sjmh_iframe()
    page.button_level()
    page.local_data()
    page.assert_att("数据分级")


@allure.feature('数据资产')
@allure.title('数据接口跳转')
def test_api_search(browser):
    page = SjmhPage(browser)
    page1 = MenuPage(browser)
    page.reback_iframe()
    page.tab_zcmh()
    page1.sjzc_sjmh_iframe()
    page.button_api()
    page.local_data()
    page.assert_att("数据接口")


@allure.feature('数据资产')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')

