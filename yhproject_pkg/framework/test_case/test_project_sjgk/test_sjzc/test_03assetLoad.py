import pytest
from yhproject_pkg.framework.data_driver.yaml_driver import load_yaml
from yhproject_pkg.framework.page.login_page.login_page import LoginPage
from yhproject_pkg.framework.page.login_page.menu_page import MenuPage

import allure
import logging

logger = logging.getLogger(__name__)


@allure.feature('资产导航')
@allure.title('登录')
@pytest.mark.parametrize('data', load_yaml('D:/PycharmProjects/pythonProject/yhproject_pkg/framework/data/login/login1.yaml'))
def test_login_data(browser, data):
    page = LoginPage(browser)
    page.driver.maximize_window()
    page.open(data["url"])
    page.input_user(data["user"])
    page.input_password(data["password"])
    page.button_remember()
    page.button_login()
    # 登录跳转
    page.switch_window(-1)
    page.assert_att("数据管控平台")
    logger.info('------------进入资产导航查看模块-------------')


@allure.feature('资产导航')
@allure.title('打开元数据菜单')
def test_meta_data(browser):
    page = MenuPage(browser)
    page.table_manage()
    page.menu_icon()
    page.menu_sjzc()
    page.sjzc_zcdh_meta()
    # 切换iframe
    page.sjzc_meta_iframe()
    page.assert_att("请选择需要查看的表")
    logger.info('01进入元数据菜单成功')


@allure.feature('资产导航')
@allure.title('打开分类菜单')
def test_classify_data(browser):
    page = MenuPage(browser)
    page.reback_iframe()
    page.table_manage()
    page.menu_icon()
    page.menu_sjzc()
    page.sjzc_zcdh_classify()
    # 切换iframe
    page.sjzc_classify_iframe()
    page.assert_att("证监会")
    logger.info('02进入数据分类菜单成功')


@allure.feature('资产导航')
@allure.title('打开分级菜单')
def test_level_data(browser):
    page = MenuPage(browser)
    page.reback_iframe()
    page.table_manage()
    page.menu_icon()
    page.menu_sjzc()
    page.sjzc_zcdh_level()
    # 切换iframe
    page.sjzc_level_iframe()
    page.assert_att("请选择需要查看的分级版本信息")
    logger.info('03进入数据分级菜单成功')


@allure.feature('资产导航')
@allure.title('打开数据标准菜单')
def test_standard_data(browser):
    page = MenuPage(browser)
    page.reback_iframe()
    page.table_manage()
    page.menu_icon()
    page.menu_sjzc()
    page.sjzc_zcdh_standard()
    # 切换iframe
    page.sjzc_standard_iframe()
    page.assert_att("请选择需要查看的标准")
    logger.info('04进入数据标准菜单成功')


@allure.feature('资产导航')
@allure.title('打开数据服务菜单')
def test_api_data(browser):
    page = MenuPage(browser)
    page.reback_iframe()
    page.table_manage()
    page.menu_icon()
    page.menu_sjzc()
    page.sjzc_zcdh_api()
    # 切换iframe
    page.sjzc_api_iframe()
    page.assert_att("请选择左侧目录树查看相关数据")
    logger.info('05进入数据接口菜单成功')


@allure.feature('资产导航')
@allure.title('退出登录')
def test_logout(browser):
    page = LoginPage(browser)
    page.reback_iframe()
    page.button_user()
    page.button_logout()
    page.wait(2)
    page.assert_att("赢和·数据管控平台")
    logger.info('.....退出登录.....')
    