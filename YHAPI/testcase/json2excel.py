import json
import pandas as pd


with open('student.json', 'r', encoding='utf-8') as f:
    data = json.load(f)

df = pd.DataFrame(data['students'])
df.to_excel('students.xlsx', index=False)
