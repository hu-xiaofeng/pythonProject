import unittest
import requests
import json
from yhapi.common.logger import logger

from yhapi.testcase.get_token import TestLogin


try:
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urljoin


class DcsApi(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.baseUrl = 'http://192.168.74.177:10018/api/query/'
        cls.token = TestLogin().token_login()

    def test_topFiveBondHold(self):
        """
        五大股票信息
        """
        url = urljoin(self.baseUrl, "sss/v1/topFiveBondHold")
        data = {
            "paging": {
                "size": 10,
                "page": 1
            }
        }
        header = {'token': self.token}
        response = requests.post(url=url, data=json.dumps(data), headers=header, timeout=3).json()
        assert response['code'] == 200
        logger.debug(f"响应结果：{response}")  # 添加响应结果日志




