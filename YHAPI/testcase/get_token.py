import unittest
import requests
import json
try:
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urljoin


class TestLogin(unittest.TestCase):

    @classmethod
    def setUp(cls):
        cls.login_url = "http://192.168.74.177:10018/ycmp-auth-center/api/auth/interface/login"
        cls.baseUrl = 'http://192.168.74.177:10018/api/query/'
        cls.appKey = "YH_admin"
        cls.secret = "64798b33e4b04451978b2a65"
        cls.header = {'Content-Type': 'application/json;charset=UTF-8'}

    def token_login(self):
        """
        测试登录
        """
        login_url = "http://192.168.74.177:10018/ycmp-auth-center/api/auth/interface/login"
        appKey = "YH_admin"
        secret = "64798b33e4b04451978b2a65"
        header = {'Content-Type': 'application/json;charset=UTF-8'}

        data = {
            'appKey': appKey,
            'secret': secret
        }

        response = requests.post(login_url, data=json.dumps(data), headers=header).json()
        assert response['code'] == 200
        assert response['message'] == 'OK'
        token = response["data"]["token"]
        return token








